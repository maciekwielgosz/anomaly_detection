###This code is to map porcesses hashes to numbers that are readable by ML algorithms###

import pyspark

df = sqlContext.sql("SELECT * from """YOUR_FILE_NAME""".txt")

from pyspark.ml.feature import StringIndexer
indexer = StringIndexer(inputCol="Process Name", outputCol="PName")
indexed = indexer.fit(df).transform(df)

from pyspark.ml.linalg import Vectors
from pyspark.ml.feature import VectorAssembler
assembler = VectorAssembler (
       inputCols=df.colums[1:-2],
       outputCol="features")

output = assembler.transform(indexed)
output.select("features","PName").show()