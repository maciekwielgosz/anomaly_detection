import numpy as np
from scipy import signal


class ExponentialSmoothing(object):
    """
         series - time series
         alpha - float [0.0, 1.0], smoothing parameter
     """

    def __init__(self, alpha):
        self.alpha = alpha

    def predict_manual(self, series):
        result = [series[0]]

        for n in range(0, len(series)):
            result.append(self.alpha * series[n] + (1 - self.alpha) * result[n])

        return np.array(result)

    def predict(self, series):
        try:
            b = np.array([self.alpha])
            a = np.array([1, self.alpha - 1])

            zi = np.array([1 - self.alpha])  # same as zi = signal.lfilter_zi(b, a)
            zi_shape = list(series.shape)
            zi_shape[0] = -1
            result, _ = signal.lfilter(b, a, series, zi=(zi*series[0]).reshape(*zi_shape), axis=0)

            return np.concatenate(([series[0]], result))
        except np.linalg.linalg.LinAlgError:
            return self.predict_manual(series)
