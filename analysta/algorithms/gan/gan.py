import math
import sys

import sklearn
import torch

import numpy as np
import torch.cuda
from torch import nn as nn, optim as optim
from tqdm import tqdm

from analysta.algorithms.gan.discriminator import Discriminator
from analysta.algorithms.gan.generator import Generator
from analysta.algorithms.gan.weights_init import weights_init
from analysta.utils.performance.torch_multi_gpu import AutoTorch
from tensorboardX import SummaryWriter


class GAN(object):
    """
    Created after https://pytorch.org/tutorials/beginner/dcgan_faces_tutorial.html
    """
    FAKE = 0
    REAL = 1

    def __init__(self,
                 max_nb_epoch,
                 x_shape,
                 gpus,
                 generator_class=Generator,
                 discriminator_class=Discriminator):
        self.__max_nb_epoch = max_nb_epoch
        self.__nz=256  # size of z latent vector (i.e. size of generator input)
        self.__lr = 0.0002  # learning rate
        self.__beta1 = 0.5  # Adam optimizer param
        self.__label_noise_margin = 0.2

        width_log = math.log2(x_shape[1])
        if width_log != int(width_log):
            raise ValueError("x_shape[1] must be a power of 2, {} is not.".format(x_shape[1]))

        self.__x_shape = (x_shape[2], x_shape[1])

        self.at = AutoTorch(gpus)
        self.at.__enter__()

        generator = generator_class(self.__nz, self.__x_shape)
        self.input_width = generator.input_width
        self.input_height = generator.input_height
        self.__generator = self.__setup_model(generator)
        self.__discriminator = self.__setup_model(discriminator_class(self.__x_shape))

        self.__criterion = nn.BCELoss()
        self.__optimizer_generator = optim.Adam(self.__generator.parameters(), lr=self.__lr, betas=(self.__beta1, 0.999))
        self.__optimizer_discriminator = optim.SGD(self.__discriminator.parameters(), lr=self.__lr)

    def __del__(self):
        if hasattr(self, '_GAN__at'):
            self.at.__exit__()

    def __setup_model(self, model):
        model_on_device = self.at.DataParallel(model.to(self.at.device))
        model_on_device.apply(weights_init)

        return model_on_device

    def fit_generator(self, train_generator, val_generator):
        history = {
            'D_loss': [],
            'G_loss': [],
            'val_D_loss': [],
            'val_D_acc': []
        }
        writer = SummaryWriter()
        for epoch in range(self.__max_nb_epoch):
            print("Epoch {}/{}".format(epoch + 1, self.__max_nb_epoch))

            losses = self.__fit_epoch(train_generator)
            discriminator_loss, generator_loss = losses[0], losses[1]
            discriminator_train_var, generator_train_var = losses[2], losses[3]
            writer.add_scalar('discriminator_train_loss: ', discriminator_loss)
            writer.add_scalar('generator_train_loss: ', generator_loss)
            writer.add_scalar('discriminator_train_var: ', discriminator_train_var)
            writer.add_scalar('generator_train_var: ', generator_train_var)
            val_loss, val_acc = self.__validate_epoch(val_generator)
            writer.add_scalar('discriminator_val_loss: ', val_loss)
            writer.add_scalar('val_acc: ', val_acc)
            # self.show_discrimnator_result(val_generator)
            # self.show_generator_result()

            history['D_loss'].append(discriminator_loss)
            history['G_loss'].append(generator_loss)
            history['val_D_loss'].append(val_loss)
            history['val_D_acc'].append(val_acc)
        
        return history

    def fit_discriminator(self, train_generator, val_generator):
        history = {
            'D_loss': [],
            'val_D_loss': [],
            'val_D_acc': []
        }
        writer = SummaryWriter()
        for epoch in range(self.__max_nb_epoch):
            print("Epoch {}/{}".format(epoch + 1, self.__max_nb_epoch))

            discriminator_loss = self.__fit_epoch_discriminator(train_generator)
            writer.add_scalar('discriminator_train_loss: ', discriminator_loss)
            val_loss, val_acc = self.__validate_epoch(val_generator)
            writer.add_scalar('discriminator_val_loss: ', val_loss)
            writer.add_scalar('val_acc: ', val_acc)

            history['D_loss'].append(discriminator_loss)
            history['val_D_loss'].append(val_loss)
            history['val_D_acc'].append(val_acc)

        return history

    def __fit_epoch(self, train_generator):
        total_batches = len(train_generator)

        generator_losses = np.zeros((total_batches, ))
        discriminator_losses = np.zeros((total_batches, ))

        with tqdm(total=total_batches, file=sys.stdout) as pbar:
            for i, data in enumerate(train_generator[:], 0):
                ############################
                # (1) Update D network: maximize log(D(x)) + log(1 - D(G(z)))
                ############################

                # Train with all-real batch
                # -------------------------
                # if i == 8:
                #     break

                self.__discriminator.zero_grad()
                # Format batch
                real_cpu = self.data_to_torch(data[0])
                batch_size = real_cpu.size(0)
                # label = torch.full((batch_size,), self.REAL, device=self.at.device)
                label = torch.FloatTensor(batch_size).uniform_(
                    self.REAL - self.__label_noise_margin, self.REAL)
                label = label.to(self.at.device)

                output = self.__discriminator(real_cpu)
                # Calculate loss on all-real batch
                err_discriminator_real = self.__criterion(output, label)
                # Calculate gradients for D in backward pass
                err_discriminator_real.backward()

                # Train with all-fake batch
                # -------------------------

                noise = self.generate_batch_of_random_vectors(batch_size)
                # Generate fake image batch with G
                fake = self.__generator(noise)
                # label.fill_(self.FAKE)
                label = torch.FloatTensor(batch_size).uniform_(
                    self.FAKE, self.FAKE + self.__label_noise_margin)
                label = label.to(self.at.device)
                # Classify all fake batch with D
                output = self.__discriminator(fake.detach())
                # Calculate D's loss on the all-fake batch
                err_discriminator_fake = self.__criterion(output, label)
                # Calculate the gradients for this batch
                err_discriminator_fake.backward()
                # Add the gradients from the all-real and all-fake batches
                err_discriminator = err_discriminator_real + err_discriminator_fake
                # Update D
                self.__optimizer_discriminator.step()

                ############################
                # (2) Update G network: maximize log(D(G(z)))
                ###########################
                self.__generator.zero_grad()
                label.fill_(self.REAL)  # fake labels are real for generator cost
                # Since we just updated D, perform another forward pass of all-fake batch through D
                output = self.__discriminator(fake).view(-1)
                # Calculate G's loss based on this output
                err_generator = self.__criterion(output, label)
                # Calculate gradients for G
                err_generator.backward()
                # Update G
                self.__optimizer_generator.step()

                # Save Losses for plotting later
                discriminator_losses[i] = err_discriminator.item()
                generator_losses[i] = err_generator.item()

                pbar.set_postfix(D_loss=discriminator_losses[i], G_loss=generator_losses[i])
                pbar.update(1)

        losses = [
            discriminator_losses.mean(),
            generator_losses.mean(),
            discriminator_losses.var(),
            generator_losses.var()
        ]

        return losses

    def generate_batch_of_random_vectors(self, batch_size):
        # Generate batch of latent vectors
        noise = torch.randn(
            batch_size,
            self.__nz,
            self.input_width,
            self.input_height,
            device=self.at.device
        )
        return noise

    def __fit_epoch_discriminator(self, train_generator):
        total_batches = len(train_generator)
        discriminator_losses = np.zeros((total_batches, ))

        with tqdm(total=total_batches, file=sys.stdout) as pbar:
            for i, data in enumerate(train_generator[:], 0):
                self.__discriminator.zero_grad()
                # Format batch
                real_cpu = self.data_to_torch(data[0])
                label = self.__labels_to_torch(data[1])
                label = label.to(self.at.device)
                output = self.__discriminator(real_cpu).view(-1)
                err_discriminator_real = self.__criterion(output, label)
                err_discriminator_real.backward()
                self.__optimizer_discriminator.step()

                # Save Losses for plotting later
                discriminator_losses[i] = err_discriminator_real.item()

                pbar.set_postfix(D_loss=discriminator_losses[i])
                pbar.update(1)

        return discriminator_losses.mean()

    def data_to_torch(self, data):
        if isinstance(data, np.ndarray):
            data_torch = torch.from_numpy(data)
            data_torch = torch.unsqueeze(data_torch, 1).float()
            data_torch = data_torch.permute(0, 1, 3, 2)
        else:
            data_torch = data
        return data_torch.to(self.at.device)

    def __labels_to_torch(self, data):
        if isinstance(data, np.ndarray):
            data_torch = torch.from_numpy(np.squeeze(data)).float()
        else:
            data_torch = data
        return data_torch.to(self.at.device)

    def __validate_epoch(self, val_generator):
        err = []
        acc = []

        with torch.no_grad():
            with tqdm(total=len(val_generator), file=sys.stdout) as pbar:
                for i, data in enumerate(val_generator[:], 0):
                    batch_x = self.data_to_torch(data[0])
                    batch_y = self.__labels_to_torch(data[1])
                    output = self.__discriminator(batch_x).view(-1)
                    batch_err = self.__criterion(output, batch_y).item()
                    batch_acc = sklearn.metrics.accuracy_score(
                        data[1],
                        1 - output.round().cpu().numpy()
                    )
                    # print("batch_acc: ", batch_acc)
                    err.append(batch_err)
                    acc.append(batch_acc)
                    pbar.set_postfix(val_D_loss=batch_err, val_D_acc=batch_acc)
                    pbar.update(1)
        print("mean acc: ", np.array(acc).mean())
        return np.array(err).mean(), np.array(acc).mean()

    def predict_generator(self, data_generator, steps=None):
        """

        :param data_generator:
        :param steps: Max no of batches to run predictions for
        :return:
        """
        predictions = []

        if steps is None:
            steps = len(data_generator)

        with tqdm(total=steps, file=sys.stdout) as pbar:
            for i, data in enumerate(data_generator[:steps], 0):
                output = self.predict_on_batch(data[0])
                predictions.append(output)
                pbar.update(1)

        return np.concatenate(predictions, axis=0)

    def predict_on_batch(self, x):
        with torch.no_grad():
            batch_x = self.data_to_torch(x)
            return np.round(1 - self.__discriminator(batch_x).view(-1).cpu().numpy())

    def show_generator_result(self):
        """

        :return:
        """
        import matplotlib.pyplot as plt
        # Generate a single batch of latent vectors
        noise = torch.randn(
            1,
            self.__nz,
            self.input_width,
            self.input_height,
            device=self.at.device
        )
        fake = self.__generator(noise).squeeze().cpu().detach().numpy()
        for i in range(fake.shape[0]):
            plt.subplot(fake.shape[0], 1, i + 1)
            plt.plot(range(len(fake[i, :])), fake[i, :])
        plt.show()

    def show_discrimnator_result(self, generator):
        """

        :param generator:
        :return:
        """
        import matplotlib.pyplot as plt
        with torch.no_grad():
            data, label = generator[0]  # pick the first batch
            # print("data shape: ", data.shape)
            output = self.__discriminator(self.data_to_torch(data)).view(-1)[0]
            # print("output: ", output.cpu().numpy())
            data = data[0]
            # print("label shape: ", label)
            plt.gcf().suptitle('label: {}, 1 - output: {}'.format(
                label[0][0],
                1 - output.cpu().numpy()
            ))

            for j in range(data.shape[1]):
                plt.subplot(data.shape[1], 1, j + 1)
                plt.plot(range(len(data[:, j])), data[:, j])

        plt.show()

    def save(self, filepath):
        torch.save(self.__discriminator, filepath)

    def load(self, filepath):
        self.__discriminator = torch.load(filepath)
        self.__discriminator.eval()



