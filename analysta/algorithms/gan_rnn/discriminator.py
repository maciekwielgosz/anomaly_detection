import torch
from torch import nn as nn


class Discriminator(nn.Module):
    def __init__(self, input_shape):
        super(Discriminator, self).__init__()

        # RNN definition
        input_size = input_shape[0]
        self.hidden_size = 128
        self.num_layers = 1

        # linear layer definition
        out_linear_size = 1

        # LSTM model definition
        self.rnn = nn.LSTM(input_size=input_size,
                           hidden_size=self.hidden_size,
                           num_layers=self.num_layers,
                           batch_first=True)

        self.linear = nn.Linear(in_features=self.hidden_size, out_features=out_linear_size)
        self.sigm = nn.Sigmoid()

    def forward(self, input):
        batch_size = input.shape[0]
        h0_zero = torch.zeros(self.num_layers, batch_size, self.hidden_size, device=input.get_device())
        c0_zero = torch.zeros(self.num_layers, batch_size, self.hidden_size, device=input.get_device())
        output_data, _ = self.rnn(input, (h0_zero, c0_zero))
        out = self.linear(output_data)
        out = self.sigm(out)[:, -1, :]
        return torch.squeeze(out, 1)
