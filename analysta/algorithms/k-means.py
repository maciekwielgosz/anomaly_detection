###PCA in conjunction with k-means is a powerful method for visualizing high dimensional data.### 

###You need to replace whatever in """X""" to match with your data###

###Captures the greatest amount of variance in the data
# Imports
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler
%config InlineBackend.figure_format='retina'



# Load in the data
df = pd.read_csv('"""YOUR_FILE_NAME.csv"""')

# Standardize the data to have a mean of ~0 and a variance of 1
X_std = StandardScaler().fit_transform(df)

# Create a PCA instance: pca
pca = PCA(n_components="""No. of Cloumns""")
principalComponents = pca.fit_transform(X_std)


# Plot the explained variances
features = range(pca.n_components_)
plt.bar(features, pca.explained_variance_ratio_, color='black')
plt.xlabel('PCA features')
plt.ylabel('variance %')
plt.xticks(features)


# Save components to a DataFrame
PCA_components = pd.DataFrame(principalComponents)


###The output will explain the majority of the variance in the data###


###Now, we need to the ideal "k" number for our k-means algorithm###
ks = range(1, 10)
inertias = []
for k in ks:
    # Create a KMeans instance with k clusters: model
    model = KMeans(n_clusters=k)
    
    # Fit model to samples
    model.fit(PCA_components.iloc[:,:3])
    
    # Append the inertia to the list of inertias
    inertias.append(model.inertia_)
    
plt.plot(ks, inertias, '-o', color='black')
plt.xlabel('number of clusters, k')
plt.ylabel('inertia')
plt.xticks(ks)
plt.show()