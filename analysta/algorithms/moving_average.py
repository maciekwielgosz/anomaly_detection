import numpy as np


class MovingAverage(object):
    """
         n - window size
     """

    def __init__(self, n=4):
        self.n = int(round(n))

    def predict(self, series):
        ret = np.cumsum(series, dtype=float)
        ret[self.n:] = ret[self.n:] - ret[:-self.n]
        return (ret[self.n - 1:] / self.n).reshape((-1, 1))
