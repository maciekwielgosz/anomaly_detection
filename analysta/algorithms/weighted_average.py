import numpy as np


class WeightedAverage(object):
    def __init__(self, weights):
        self.weights = np.flip(weights, axis=0)
        self.total = np.sum(self.weights)

    def predict(self, series):
        tmp = []
        pred_len = series.shape[0] - self.weights.size
        for idx in range(self.weights.size):
            tmp.append(series[idx:pred_len + idx + 1])
        tmp = np.vstack(tmp).T
        return np.dot(tmp, self.weights) / self.total
