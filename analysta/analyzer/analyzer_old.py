from __future__ import print_function
import numpy

from analysta.analyzer.anomaly_candidate import AnomalyCandidate
from analysta.data.utils import unwrap_one_hot


class Analyzer(object):
    DEFAULT_RULES = [{'length': 0}]

    def __init__(self, edges, rules=None):
        """

        :param edges: Array of edges for out grid
        :param rules: Rules to use when determining if a candidate is an anomaly. By default [{'length': 0}] is used.
                   Possible rule names:
                     * 'length' - compares (>) anomaly length with length_th
                     * 'cum_amplitude' - compares (>) anomaly cum_amplitude with cum_amplitude threshold calculated using cum_amplitude_th_mode
                     * 'amplitudes' - compares (>) amplitudes of all samples in a candidate with amplitudes_th
                     * 'max_amplitude' - compares (>) maximum amplitude in a candidate with max_amplitude_th
                   Rules in the inner list are connected by AND, while those in the outer list are connected by OR.

                   Examples:

                   >>> [{'length': 2, 'max_amplitude': 0.5}, {'cum_amplitude': 0.9}]
                   will mark a candidate as an anomaly if both its length and maximum amplitude is bigger than relevant
                   thresholds (2 and 0.5) OR if its cum_amplitude is bigger than 0.9

        :type rules: list(dict(str: number)) or None
        """
        self.__edges = edges
        self.__middles = Analyzer.get_middles_for_edges(self.__edges)

        self.__possible_thresholds = [
            'length',
            'max_amplitude',
            'amplitudes',
            'cum_amplitude'
        ]

        self.__rules = Analyzer.DEFAULT_RULES if rules is None else rules

    def __get_rules(self):
        return self.__rules[:]

    def __set_rules(self, rules):
        for or_rule in rules:
            for and_rule in or_rule.keys():
                if and_rule not in self.__possible_thresholds:
                    raise ValueError('{} is not a possible threshold'.format(and_rule))
        self.__rules = rules[:]

    rules = property(__get_rules, __set_rules)

    def find_anomaly_candidates(self, real_y, pred_y):
        # TODO: finish refactor so detection can work with continuously arriving samples
        bundled_stats = []
        anomaly_candidate = []
        stats = None
        trigger_point = None
        is_anomaly = False

        unwrapped_real_y = unwrap_one_hot(real_y)
        unwrapped_pred_y = unwrap_one_hot(pred_y)

        for i in range(0, len(unwrapped_real_y)):
            r = unwrapped_real_y[i]
            p = unwrapped_pred_y[i]

            if numpy.abs(r-p).any():
                anomaly_candidate.append((i, r, p))
                is_anomaly, stats = self.__is_anomaly(anomaly_candidate)
                if is_anomaly and trigger_point is None:
                    trigger_point = len(anomaly_candidate) - 1
            else:
                if stats is not None:
                    bundled_stats.append(AnomalyCandidate(
                        anomaly_candidate[0][0], anomaly_candidate[-1][0] + 1, is_anomaly, stats, trigger_point))
                anomaly_candidate = []
                stats = None
                trigger_point = None

        return bundled_stats

    @staticmethod
    def find_thresholds_and_rules(bundled_stats):
        if not len(bundled_stats):
            return Analyzer.DEFAULT_RULES

        keys = bundled_stats[0].stats.keys()

        # convert to numpy arrays
        stats = {}
        for key in keys:
            item = [s.stats[key] for s in bundled_stats]
            stats[key] = numpy.array(item)

        # flatten amplitudes values
        stats['amplitudes'] = numpy.concatenate(stats['amplitudes'])

        eng_edges, _ = Analyzer.get_edges(stats['cum_amplitude'], 'sturges')
        amp_edges, _ = Analyzer.get_edges(stats['max_amplitude'], 'sturges')

        maxima = {
            'length': numpy.amax(stats['length']),
            'cum_amplitude': len(eng_edges),
            'amplitudes': len(amp_edges),  # same as for max_amplitudes
            'max_amplitude': len(amp_edges)
        }

        # brute force
        best = (0, maxima['length'], 0, 0, 0)
        for len_th in range(maxima['length']-1, -1, -1):
            for eng_th in range(maxima['cum_amplitude']-1, -1, -1):
                for amp_th in range(maxima['amplitudes']-1, -1, -1):
                    for mamp_th in range(maxima['max_amplitude']-1, -1, -1):
                        len_ok = stats['length'] <= len_th
                        eng_ok = stats['cum_amplitude'] <= (eng_edges[eng_th - 1] if eng_th else 0)
                        amp_ok = numpy.all(stats['amplitudes'] <= (amp_edges[amp_th - 1] if amp_th > 0 else 0))
                        mamp_ok = stats['max_amplitude'] <= (amp_edges[mamp_th - 1] if mamp_th > 0 else 0)

                        all_in = numpy.all(numpy.logical_or(
                            numpy.logical_or(len_ok, eng_ok), numpy.logical_or(amp_ok, mamp_ok)
                        ))

                        if all_in:
                            count = (maxima['length'] - len_th) * (maxima['cum_amplitude'] - eng_th) * \
                                    (maxima['amplitudes'] - amp_th) * (maxima['max_amplitude'] - mamp_th)
                            if count > best[0]:
                                best = (count, len_th, eng_th, amp_th, mamp_th)
                        else:
                            break
        thresholds = {}
        if best[1] > 0:
            thresholds['length'] = best[1]
        if best[2] > 0:
            thresholds['cum_amplitude'] = eng_edges[best[2] - 1]
        if best[3] > 0:
            thresholds['amplitudes'] = amp_edges[best[3] - 1]
        if best[4] > 0:
            thresholds['max_amplitude'] = amp_edges[best[4] - 1]

        result = [
            {'length': maxima['length']},
            {'cum_amplitude': eng_edges[maxima['cum_amplitude'] - 1]},
            {'max_amplitude': amp_edges[maxima['max_amplitude'] - 1]},
            thresholds
        ]

        return result

    @staticmethod
    def get_edges(data, bins='max', force_max=False):
        """

        :param data:
        :param bins: 'max'|'auto'|'fd'|'doane'|'scott'|'rice'|'sturges'|'sqrt';
                     falls back to 'auto' if it determines that 'max' is invalid
                     'max' - find a maximum value in samples and treat it as a number of bins; range will start from 1
        :param force_max: force 'max' algorithm even if maximum value in data >= 25
        :return:
        """
        max_in_data = 0
        if bins == 'max':
            max_in_data = numpy.amax(data)

        if bins == 'max' and max_in_data > 1 and (max_in_data < 25 or force_max):
            _, edges = numpy.histogram(data, bins=max_in_data, range=[1, max_in_data])
            labels = numpy.arange(1, max_in_data + 1)
        else:
            # see https://docs.scipy.org/doc/numpy/reference/generated/numpy.histogram.html#numpy-histogram
            correct_values = ['auto', 'fd', 'doane', 'scott', 'rice', 'sturges', 'sqrt']
            bins = bins if bins in correct_values else 'auto'
            _, edges = numpy.histogram(data, bins=bins)
            labels = numpy.round(edges, 3)

        return edges, labels

    def __is_anomaly(self, anomaly_candidate):
        if not len(anomaly_candidate):
            return False

        candidate = numpy.array(anomaly_candidate).T
        real = candidate[1]
        pred = candidate[2]

        length = candidate.shape[1]
        amplitudes = Analyzer.get_amplitudes(real, pred, self.__middles)
        cum_amplitude = Analyzer.get_cum_amplitude(amplitudes)
        max_amplitude = Analyzer.get_max_amplitude(amplitudes)

        stats = {
            'length': length,
            'cum_amplitude': cum_amplitude,
            'amplitudes': amplitudes,
            'max_amplitude': max_amplitude
        }

        is_anomaly = self.__apply_rules(stats)

        return is_anomaly, stats

    def __apply_rules(self, stats):
        is_anomaly = False

        for or_rule in self.__rules:
            r = True
            for and_rule_key, and_rule_value in or_rule.items():
                if isinstance(stats[and_rule_key], numpy.ndarray):
                    t = numpy.all(stats[and_rule_key] > and_rule_value)
                else:
                    t = stats[and_rule_key] > and_rule_value
                r = r and t

            is_anomaly = is_anomaly or r
        return is_anomaly

    @staticmethod
    def __get_middles_or_numbers(middles, real):
        return middles[real] if middles is not None else real

    @staticmethod
    def get_cum_amplitude(amplitudes):
        return numpy.sum(amplitudes)

    @staticmethod
    def get_amplitudes(real, pred, middles=None):
        real_val = Analyzer.__get_middles_or_numbers(middles, real)
        pred_val = Analyzer.__get_middles_or_numbers(middles, pred)

        return tuple(numpy.abs(numpy.diff(numpy.concatenate((
            real_val,
            pred_val
        )), axis=0)))

    @staticmethod
    def get_max_amplitude(amplitudes):
        return numpy.amax(amplitudes)

    @staticmethod
    def get_middles_for_edges(edges):
        if edges is not None and edges[0] is not None:
            edges = numpy.concatenate(([[0]], edges, [[1]]), axis=1)
            return numpy.array([numpy.mean(edges[0, i:i+2]) for i in range(edges.shape[1] - 1)])
        else:
            return None
