class AnomalyCandidate(object):
    """
    Data structure to hold info about anomaly candidate
    """

    __slots__ = ('__start', '__stop', '__is_anomaly', '__stats', '__trigger_point')

    def __init__(self, start, stop, is_anomaly, stats, trigger_point):
        self.__start = start
        self.__stop = stop
        self.__is_anomaly = is_anomaly
        self.__stats = dict(frozenset(stats.items()))
        self.__trigger_point = trigger_point

    def __get_start(self):
        return self.__start
    start = property(__get_start)

    def __get_stop(self):
        return self.__stop
    stop = property(__get_stop)

    def __get_is_anomaly(self):
        return self.__is_anomaly
    is_anomaly = property(__get_is_anomaly)

    def __get_trigger_point(self):
        return self.__trigger_point
    trigger_point = property(__get_trigger_point)

    def __get_stats(self):
        # return a copy
        return dict(frozenset(self.__stats.items()))
    stats = property(__get_stats)

    def to_tuple(self):
        return self.start, self.stop, self.is_anomaly, self.stats, self.trigger_point

    @staticmethod
    def from_tuple(a):
        return AnomalyCandidate(a[0], a[1], a[2], a[3], a[4])
