import numpy

from analysta.analyzer.plugins.plugin import AnalyzerPlugin


class MaxSquaredErrorPlugin(AnalyzerPlugin):
    """
    Decide if whole chunk is anomalous based on remembered threshold.
    """

    def __init__(self, *args, **kwargs):
        super(MaxSquaredErrorPlugin, self).__init__(*args, **kwargs)
        self.__threshold = self._config.get('analyzer.max_squared_error.threshold')
        if self.__threshold is not None:
            self._needs_fitting = False
            self.__threshold = numpy.array(self.__threshold)

    def fit_on_batch(self, real_y, predicted_y, real_samples_labels, real_chunks_labels):
        if self.__threshold is None:
            self.__threshold = numpy.zeros((real_y.shape[-1],))

        normal_indices = numpy.nonzero(real_samples_labels == 0)
        squares = numpy.square(
            real_y[normal_indices] - predicted_y[normal_indices],
            dtype=numpy.float128
        )
        if len(squares):
            max_errors = numpy.max(squares, axis=0)

            self.__threshold = numpy.maximum(self.__threshold, max_errors)

    def all_training_data_shown(self):
        self._config['analyzer.max_squared_error.threshold'] = self.__threshold.tolist()

    def predict_on_batch(self, real_y, predicted_y):
        squares = numpy.square(
            real_y - predicted_y,
            dtype=numpy.float128
        ).reshape(tuple([-1] + list(real_y.shape[2:])))
        channels_labels = squares > self.__threshold
        samples_labels = self._channels_to_points_labels(channels_labels)

        return samples_labels.reshape(real_y.shape[:2])
