import argparse
import os


def readable_file(x):
    if not os.access(x, os.R_OK):
        argparse.ArgumentTypeError('Cannot open %s!' % (x,))
    return x


def writable_dir(x):
    if not os.path.exists(x):
        try:
            os.mkdir(x, 0o700)
        except OSError:
            argparse.ArgumentTypeError('Cannot create %s!' % (x,))
    if not os.access(x, os.W_OK):
        argparse.ArgumentTypeError('Cannot write to %s!' % (x,))
    return x