import logging

from analysta.cli.data.run_autocorrelation import add_autocorrelation_parser
from analysta.cli.data.run_batches import add_batches_parser
from analysta.cli.data.run_cleaning import add_cleaning_parser
from analysta.cli.data.run_correlation import add_correlation_parser
from analysta.cli.data.run_missing import add_missing_parser
from analysta.cli.data.run_stats import add_stats_parser
from analysta.cli.data.run_subset import add_subset_parser
from analysta.utils import get_definition_from_path


def add_parser_args(parser):
    subparsers = parser.add_subparsers(dest='mode')

    add_batches_parser(subparsers)
    add_autocorrelation_parser(subparsers)
    add_subset_parser(subparsers)
    add_cleaning_parser(subparsers)
    add_correlation_parser(subparsers)
    add_missing_parser(subparsers)
    add_stats_parser(subparsers)


def main(args):
    try:
        run_func = get_definition_from_path(
            'analysta.cli.data.run_{}.run_{}'.format(args.mode, args.mode),
            'run function'
        )
        run_func(**vars(args))
    except ModuleNotFoundError:
        logging.getLogger(__name__).critical('Unknown "data" mode')
        raise
