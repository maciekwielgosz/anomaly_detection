import argparse

from analysta.cli.arguments.common import add_config_argument, add_results_argument
from analysta.cli.data.utils import setup_for_analyzing
from analysta.visualization.autocorrelation import plot_batch_autocorrelation


def run_autocorrelation(config, results_dir, **kwargs):
    analyzed_channels, batches_limit, \
        channels_names, train_data = setup_for_analyzing(config, 1, True, True)

    batch = train_data[0]
    plot_batch_autocorrelation(batch[0], 0, analyzed_channels, channels_names, batch[2], results_dir)


def add_autocorrelation_parser(subparsers=None):
    description = 'calculate autocorrelation for single, randomized batch'

    if subparsers is not None:
        parser = subparsers.add_parser('autocorrelation', help=description)
    else:
        parser = argparse.ArgumentParser(description=description)

    add_config_argument(parser)
    add_results_argument(parser)
