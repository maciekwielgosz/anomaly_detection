import argparse
import logging

import numpy as np
from tqdm import tqdm
import sys

from analysta.cli.arguments.common import add_config_argument, add_results_argument, add_limit_argument
from analysta.cli.data.utils import setup_for_analyzing
from analysta.data.analysis.mean_and_var import get_mean_and_variance_for_batch, \
    update_mean_and_variance_for_dataset
from analysta.metrics import mean_absolute_percentage_error


def run_batches(config, results_dir, limit=None, random_batches=False, threshold=1.0, **kwargs):
    from analysta.visualization.percent_change import plot_percent_change

    logger = logging.getLogger(__name__)
    analyzed_channels, batches_limit, \
        channels_names, train_data = setup_for_analyzing(config, limit, random_batches)

    means_history = np.zeros((batches_limit, len(analyzed_channels)), dtype=np.float32)
    vars_history = np.zeros((batches_limit, len(analyzed_channels)), dtype=np.float32)

    dataset_mean = np.zeros((len(analyzed_channels), ))
    dataset_var = np.zeros((len(analyzed_channels), ))
    dataset_size = 0

    evaluated_batches = 0
    error = float('inf')
    stop = False

    with tqdm(total=batches_limit, file=sys.stdout) as pbar:
        for i, batch in enumerate(train_data[:batches_limit]):
            batch_mean, batch_var = get_mean_and_variance_for_batch(batch[0])
            batch_size = batch[0].shape[0]

            dataset_mean, dataset_var, dataset_size = update_mean_and_variance_for_dataset(
                dataset_mean, dataset_var, dataset_size,
                batch_mean, batch_var, batch_size
            )

            means_history[i] = dataset_mean
            vars_history[i] = dataset_var

            if i > 0:
                error = mean_absolute_percentage_error(dataset_mean, means_history[i-1])
                if error < threshold:
                    stop = True

            evaluated_batches = i
            pbar.set_postfix(error=error)
            pbar.update(1)

            if stop:
                break

    # flush tqdm
    sys.stdout.flush()

    if evaluated_batches > 1:
        plot_percent_change(analyzed_channels, channels_names, means_history[:evaluated_batches], dataset_mean,
                            results_dir, 'dataset_online_mean.pdf', threshold)
        plot_percent_change(analyzed_channels, channels_names, vars_history[:evaluated_batches], dataset_var,
                            results_dir, 'dataset_online_var.pdf', threshold)

    logger.info('search stopped after {} batches (limit: {})'.format(
        evaluated_batches + 1, batches_limit
    ))
    for i, ch in enumerate(analyzed_channels):
        logger.info('{} channel: mean={:.8f}, var={:.8f}'.format(
            channels_names[ch],
            dataset_mean[i],
            dataset_var[i]
        ))


def add_batches_parser(subparsers=None):
    description = 'find the minimum number of batches for the further experiments'

    if subparsers is not None:
        parser = subparsers.add_parser('batches', help=description)
    else:
        parser = argparse.ArgumentParser(description=description)

    add_config_argument(parser)
    add_results_argument(parser)
    add_limit_argument(parser)
    parser.add_argument(
        '--random-batches',
        '-x',
        action='store_true',
        help='randomizes batches'
    )
    parser.add_argument(
        '--threshold',
        '-t',
        type=float,
        help='stop if MAPE between current and previous batch falls below the threshold (0.0-100.0)',
        default=1.0
    )
