import logging
import sys

import humanize

from analysta.data.loading import load_series_from_paths, get_series_loader, get_anomaly_marks, get_marks_for_paths
from analysta.data.sequences.data_sequence import DataSequence
from analysta.utils.config.get_config_instance import get_config_instance


def setup_for_analyzing(config_path, batches_limit, shuffle_batches=True, return_indexes=False):
    config = get_config_instance(config_path)
    analyzed_channels = list(set(config.get('data.input_channels', []) + config.get('data.output_channels', [])))
    channels_names = config.get('data.channels_names', [])

    batches_limit, train_data = setup(config, 'data.train_paths', batches_limit, analyzed_channels, [], shuffle_batches,
                                      return_indexes)

    return analyzed_channels, batches_limit, channels_names, train_data


def setup(config, paths_key, batches_limit, input_channels, output_channels, shuffle_batches, return_indexes,
          normalize=True):
    """

    :param config: Config
    :param paths_key: str
    :param batches_limit: int
    :param input_channels: list
    :param output_channels: list
    :param shuffle_batches: bool
    :param return_indexes: bool
    :param normalize: bool
    :return:
    """
    logger = logging.getLogger(__name__)
    series_loader = get_series_loader(config)
    paths = config.load_paths(paths_key)
    raw_data, _ = load_series_from_paths(paths, series_loader)
    raw_anomalies = get_anomaly_marks(config)
    anomaly_marks = get_marks_for_paths(raw_anomalies, paths)

    look_ahead = config.get('preparation.look_ahead', 1)
    look_back = config.get('preparation.look_back', 256)
    batch_size = config.get('preparation.batch_size', 1024)
    samples_percentage = config.get('preparation.samples_percentage', 1)

    data_sequence = DataSequence(
        data=raw_data,
        config=config,
        look_ahead=look_ahead,
        look_back=look_back,
        batch_size=batch_size,
        samples_percentage=samples_percentage,
        shuffle=shuffle_batches,
        allow_random=shuffle_batches,
        return_indexes=return_indexes,
        input_channels=input_channels,
        output_channels=output_channels,
        anomaly_marks=anomaly_marks,
        normalize=normalize
    )

    total_batches = len(data_sequence)
    batches_limit = min(total_batches, batches_limit) if batches_limit is not None else total_batches
    samples_limit = min(data_sequence.total_samples, batches_limit * batch_size)
    logger.info('Run for {} samples in {} batches'.format(
        humanize.intcomma(samples_limit),
        humanize.intcomma(batches_limit)
    ))
    logger.info('batch_size={}, look_back={}'.format(
        humanize.intcomma(batch_size),
        humanize.intcomma(look_back)
    ))
    # flush logger messages before tqdm progress bar is displayed
    sys.stdout.flush()

    return batches_limit, data_sequence
