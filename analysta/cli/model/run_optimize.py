import argparse
import logging
import warnings


from analysta.cli.arguments.common import add_config_argument, add_results_argument
from analysta.cli.arguments.types import readable_file
from analysta.models.interfaces.nn_model import NNModel
from analysta.optimization.space_utils import get_optimization_space, update_config_with_space_kwargs
from analysta.optimization.objective import objective
from analysta.utils import get_definition_from_path, get_results_path
from analysta.utils.config import Config
from analysta.utils.config.get_config_instance import get_config_instance


def add_optimize_parser(subparsers=None):
    description = 'run optimizer for model defined in CONFIG_FILE.JSON with params defined in OPTIMIZER_FILE.JSON'

    if subparsers is not None:
        parser = subparsers.add_parser('optimize', help=description)
    else:
        parser = argparse.ArgumentParser(description=description)

    add_config_argument(parser)

    parser.add_argument(
        '-o',
        '--optimizer',
        metavar='OPTIMIZER_FILE.JSON',
        type=readable_file,
        required=True
    )

    add_results_argument(parser)

    parser.add_argument(
        '-m',
        '--mute-detector',
        action='store_true'
    )

    parser.add_argument(
        '-p',
        '--checkpoint',
        metavar='CHECKPOINT.PKL',
        type=readable_file,
        required=False,
        default=None
    )


def run_optimize(config, optimizer, results_dir, mute_detector=False, checkpoint=None, **kwargs):
    # only require them if user wants to optimize
    from skopt.utils import use_named_args
    from skopt.callbacks import CheckpointSaver
    from skopt import load

    logger = logging.getLogger(__name__)

    if mute_detector:
        effective_level = logger.getEffectiveLevel()

        logging.getLogger('analysta').setLevel(logging.WARNING)  # silence all of analysta
        logger.setLevel(effective_level)  # enable self logging
        logging.getLogger('analysta.optimization').setLevel(effective_level)  # enable optimization logging

    logger.info('Optimization starting...')

    writer_wrapper = [None]

    config_instance = get_config_instance(config)
    optimizer_instance = Config(optimizer)

    optimizer_function = get_definition_from_path(optimizer_instance.get('function'), 'function')

    space = get_optimization_space(config_instance, optimizer_instance)

    @use_named_args(space)
    def objective_wrapper(**kwargs):
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            return objective(config, writer_wrapper, results_dir,
                             optimizer_instance.get('metric'), optimizer_instance.get('reverse_metric', False),
                             **kwargs)

    optimizer_kwargs = optimizer_instance.get('function_kwargs', {})

    if 'n_jobs' not in optimizer_kwargs.keys():
        model_class = get_definition_from_path(config_instance['setup.model'], 'Model')
        optimizer_kwargs['n_jobs'] = 1 if issubclass(model_class, NNModel) else -1

    if checkpoint is not None:
        checkpoint_instance = load(checkpoint)
        x0 = checkpoint_instance.x_iters
        y0 = checkpoint_instance.func_vals
    else:
        x0 = None
        y0 = None

    checkpoint_saver = CheckpointSaver("./checkpoint.pkl", compress=9)

    opt = optimizer_function(objective_wrapper,
                             x0=x0,
                             y0=y0,
                             dimensions=space,
                             #callback=[checkpoint_saver],
                             **optimizer_kwargs
                             )

    logger.info('Optimization done')

    logger.info('Saving best config...')
    update_config_with_space_kwargs(config_instance, dict(zip([
        dimension.name
        for dimension in space
    ], opt.x)))
    best_path = get_results_path('best-config', ext='json', subdir_name=results_dir)
    config_instance.dump(best_path)
    logger.info('Best config saved')

    return config_instance
