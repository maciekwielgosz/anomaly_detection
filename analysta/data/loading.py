import csv
import itertools
import os

import numpy
from numpy.lib.npyio import NpzFile


def series_loader(file_path, mmap_mode='r'):
    """
    Default loader for series stored as *.npz, *.npy or *.csv files. For *.npy files tries to use memmap.

    :param file_path: File containing the series data
    :param mmap_mode: memmap mode; 'r' by default; pass None to load data into RAM
    :return: Loaded series data
    """
    if file_path.endswith('.csv'):
        series_data = numpy.genfromtxt(file_path, delimiter=',', skip_header=1)
    else:
        series_data = numpy.load(file_path, mmap_mode=mmap_mode, allow_pickle=True)
        if isinstance(series_data, NpzFile):
            series_data = series_data['data']

    if series_data.ndim == 1:
        series_data = series_data.reshape((-1, 1))
    return series_data


def load_series_from_paths(paths, loader=None, mmap_mode='r'):
    if loader is None:
        loader = series_loader
    data = []
    for fp in paths.values():
        data.append(loader(fp, mmap_mode))
    if len(data) and isinstance(data[0], list):
        # special case when loader returns multiple series
        orig_keys = list(paths.keys())
        keys = ['{}_{}'.format(orig_keys[j], i) for j in range(len(data)) for i in range(len(data[j]))]
        data = list(itertools.chain.from_iterable(data))
        return data, keys
    return data, paths.keys()


def get_anomaly_marks(config):
    anomaly_marks_paths = config.load_paths('data.anomaly_marks_paths')
    anomaly_marks = {}

    for path in anomaly_marks_paths.values():
        with open(path, newline='') as amf:
            reader = csv.DictReader(amf)
            for row in reader:
                if row['name'] not in anomaly_marks.keys():
                    anomaly_marks[row['name']] = []
                anomaly_marks[row['name']].append((
                    int(row['start']),
                    int(row['stop'])
                ))
    return anomaly_marks


def get_marks_for_paths(raw_anomalies, paths):
    marks = []
    for path in paths:
        name = extract_series_name_from_path(path)
        marks.append(raw_anomalies.get(name, []))
    return marks


def get_series_loader(config):
    series_loader_path = config.get('data.series_loader')
    loader = None
    if series_loader_path is not None:
        import importlib.util
        try:
            spec = importlib.util.spec_from_file_location("series_loader", series_loader_path)
            imported_module = importlib.util.module_from_spec(spec)
            spec.loader.exec_module(imported_module)
        except AttributeError:
            imported_module = importlib.import_module(
                os.path.basename(series_loader_path)
            )
        loader = imported_module.series_loader
    return loader


def extract_series_name_from_path(data_path):
    return os.path.join(
        os.path.basename(os.path.dirname(data_path)),  # last dir
        os.path.splitext(os.path.basename(data_path))[0]  # file name w/o extension
    )
