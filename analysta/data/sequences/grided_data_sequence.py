import sys

import numpy as np
from sklearn.cluster import KMeans
from tqdm import tqdm

from analysta.data.sequences.data_sequence import DataSequence
from analysta.data.utils import convert_to_grid, get_edges


class GridedDataSequence(DataSequence):
    def __init__(self, *args, **kwargs):
        super(GridedDataSequence, self).__init__(*args, **kwargs)

        self.__in_edges = None
        self.__out_edges = None

        self.__in_buckets = self._config.setdefault('preparation.in_buckets', 0)
        self.__out_buckets = self._config.setdefault('preparation.out_buckets', 0)
        self.__in_bucketization = self._config.setdefault('preparation.in_bucketization', 'none')
        self.__out_bucketization = self._config.setdefault('preparation.out_bucketization', 'none')

        self.__copy_shared_edges = (self.__in_buckets == self.__out_buckets) and (self.__in_bucketization == self.__out_bucketization)

        self.__max_kmeans_iter = self._config.setdefault('preparation.max_kmeans_iter', 300)

        if self._config.get('preparation.edges_path'):
            edges = np.load(self._config['preparation.edges_path'], allow_pickle=True)
            self.__in_edges = edges['in_edges']
            self.__out_edges = edges['out_edges']
        else:
            if self.__in_buckets == 0:
                self.__in_edges = [None] * self.shape_x[1]
            if self.__out_buckets == 0:
                self.__out_edges = [None] * self.shape_y[0]

        if self.__out_buckets > 0:
            self.setup_one_hot({
              str(ch): {
                  "l{}".format(i): i for i in range(self.__out_buckets)
              } for ch in self._output_channels
            })

    def _postprocess_batch(self, batch_x, batch_y):
        processed_x = batch_x
        processed_y = batch_y

        if self.__in_buckets == 0 or self.__out_buckets == 0:
            super_x, super_y = super(GridedDataSequence, self)._postprocess_batch(batch_x, batch_y)

            if self.__in_buckets == 0:
                processed_x = super_x
            if self.__out_buckets == 0:
                processed_y = super_y

        if self.__in_buckets > 0:
            processed_x = convert_to_grid(batch_x, self.in_edges) / self.__in_buckets  # convert + normalize grid
        if self.__out_buckets > 0:
            processed_y = convert_to_grid(batch_y, self.out_edges)

        return (
            processed_x,
            processed_y
        )

    @property
    def in_edges(self):
        if self.__in_edges is None:
            self.__in_edges, out_edges = self.__get_edges(True, self.__out_edges is None)
            if self.__out_edges is None:
                self.__out_edges = out_edges
        return self.__in_edges

    @in_edges.setter
    def in_edges(self, value):
        self.__in_edges = value

    @property
    def out_edges(self):
        if self.__out_edges is None:
            in_edges, self.__out_edges = self.__get_edges(self.__in_edges is None, True)
            if self.__in_edges is None:
                self.__in_edges = in_edges
        return self.__out_edges

    @out_edges.setter
    def out_edges(self, value):
        self.__out_edges = value

    def __get_edges(self, calc_in=True, calc_out=True):
        exclude = None
        if self.__copy_shared_edges:
            exclude = self._shared_channels

        total_batches = len(self._reader)

        in_edges_list = [[] for _ in range(self.shape_x[1])]
        out_edges_list = [[] for _ in range(self.shape_y[0])]

        self._logger.info("Starting edges search...")
        with tqdm(total=total_batches, file=sys.stdout) as pbar:
            for batch_idx in range(0, total_batches):
                batch_x, batch_y, _ = self._reader.calc_item(batch_idx)
                in_edges = get_edges(batch_x, self.__in_buckets, self.__in_bucketization) if calc_in else None
                out_edges = get_edges(
                    batch_y, self.__out_buckets, self.__out_bucketization, exclude=exclude
                ) if calc_out else None

                for ch, l in enumerate(in_edges_list):
                    l.append(in_edges[ch] if in_edges is not None else None)
                for ch, l in enumerate(out_edges_list):
                    l.append(out_edges[ch] if out_edges is not None else None)

                pbar.update(1)

                if not np.any(in_edges) and not np.any(out_edges):
                    break

        sys.stdout.flush()

        if len(in_edges_list) and len(in_edges_list[0]) > 1:
            self._logger.info("Running KMeans for input edges...")
            final_in_edges = self.__cluster_edges(in_edges_list)
        else:
            final_in_edges = [v[0] for v in in_edges_list]

        if len(out_edges_list) and len(out_edges_list[0]) > 1:
            self._logger.info("Running KMeans for output edges...")
            final_out_edges = self.__cluster_edges(out_edges_list)
        else:
            final_out_edges = [v[0] for v in out_edges_list]

        if self.__copy_shared_edges and calc_out:
            self.__copy_in_to_out_edges(final_in_edges, final_out_edges)

        self._logger.info("Edges calculated")

        return final_in_edges, final_out_edges

    def __copy_in_to_out_edges(self, in_edges, out_edges):
        # no point in calculating twice + input edges are more accurate (maybe?)
        for out_idx, in_idx in enumerate(self._shared_channels):
            if in_idx > -1:
                out_edges[out_idx] = in_edges[in_idx]

    def __cluster_edges(self, edges_list):
        new_edges = []
        for ch in range(len(edges_list)):
            if edges_list[ch][0] is not None:
                edges_data = np.array(edges_list[ch]).reshape((-1, 1))
                kmeans = KMeans(
                    n_clusters=len(edges_list[ch][0]),
                    max_iter=self.__max_kmeans_iter,
                    verbose=0,
                    n_jobs=-1,
                    n_init=8
                )
                kmeans.fit(edges_data)
                new_edges.append(np.sort(kmeans.cluster_centers_.reshape((-1,))))
                self._logger.debug("Channel {}: {}".format(
                    ch, np.sort(kmeans.cluster_centers_.reshape((-1,)))
                ))
            else:
                new_edges.append(None)

        return new_edges

    def sync_with(self, other_data_sequence):
        super(GridedDataSequence, self).sync_with(other_data_sequence)

        self.in_edges = other_data_sequence.in_edges
        self.out_edges = other_data_sequence.out_edges

    @property
    def in_buckets(self):
        return self.__in_buckets

    @property
    def out_buckets(self):
        return self.__out_buckets

    @property
    def in_bucketization(self):
        return self.__in_bucketization

    @property
    def out_bucketization(self):
        return self.__out_bucketization
