import numpy as np

from .flat_data_sequence import FlatDataSequence


class OutliersFlatDataSequence(FlatDataSequence):
    def __init__(self, *args, **kwargs):
        super(OutliersFlatDataSequence, self).__init__(*args, **kwargs)
        self.setup_one_hot(False)
        self.__mapping = np.array(self._config.get('data.mapping', [1, -1]))

    def _postprocess_batch(self, batch_x, batch_y):
        batch_x, batch_y = super(OutliersFlatDataSequence, self)._postprocess_batch(batch_x, batch_y)

        batch_y = self.__mapping[batch_y]

        return batch_x, batch_y

    def inverse_mapping(self, batch_y):
        return batch_y == self.__mapping[1]
