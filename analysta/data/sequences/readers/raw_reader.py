import logging
import numpy

from analysta.data.sequences.readers.reader import Reader

# how much of available memory can single indexes array maximally use?
# relevant if shuffle=True and allow_random=True
# if the threshold is crossed, instead of shuffling will try to use random
INDEXES_MEMORY_THRESHOLD = 0.25


class RawReader(Reader):
    """
    Do not use directly, use DataSequence instead
    """

    def __init__(self, look_ahead, required_history, shuffle,
                 allow_random, anomaly_marks=None, **kwargs):
        self.__required_history = required_history if required_history is not None else kwargs['look_back']
        self.__look_ahead = look_ahead
        self.__look_back_padding = self.__get_look_back_padding(kwargs['look_back'])

        super(RawReader, self).__init__(**kwargs)

        self.__shuffle = shuffle
        self.__use_random = shuffle and allow_random and self.__would_produce_huge_indexes_array()

        self.__indexes = None

        self.__anomaly_marks = anomaly_marks
        self.__in_marks = False
        self.__out_marks = False
        self.__label_marks = False

        self.__current_batch_indexes = None

        # TODO: refactor repetitive code here and in __insert_*_marks

        if 'marks' in self._in_channels:
            if self.__anomaly_marks is None:
                self._logger.critical('Requested to use "marks" in input channels, but no marks data available.')
                exit(1)
            self.__in_marks = self._in_channels.index('marks')
            self._in_channels.remove('marks')
            if any([isinstance(ch, str) for ch in self._in_channels]):
                raise ValueError('Invalid input channel value, only integers and "marks" string are allowed.')

        if 'marks' in self._out_channels:
            if self.__anomaly_marks is None:
                self._logger.critical('Requested to use "marks" in output channels, but no marks data available.')
                exit(1)
            self.__out_marks = self._out_channels.index('marks')
            self._out_channels.remove('marks')
            if any([isinstance(ch, str) for ch in self._out_channels]):
                raise ValueError('Invalid output channel value, only integers and "marks" string are allowed.')

        if self._labels_channel == 'marks':
            if self.__anomaly_marks is None:
                self._logger.critical('Requested to use "marks" in labels channel, but no marks data available.')
                exit(1)
            self.__label_marks = True
        elif isinstance(self._labels_channel, str):
            raise ValueError('Invalid labels channel value, only integers and "marks" string are allowed.')

    def __would_produce_huge_indexes_array(self):
        import psutil

        indexes_dtype = numpy.array([self._total_usable_samples]).dtype
        indexes_size_in_bytes = indexes_dtype.itemsize * self._total_usable_samples / self._chunk_size
        mem_info = psutil.virtual_memory()

        if indexes_size_in_bytes > mem_info.available * INDEXES_MEMORY_THRESHOLD:
            self._logger.info('{} "{}" will use random batches.'.format(
                self.__class__.__name__,
                self._name
            ))
            return True

        return False

    def __get_look_back_padding(self, look_back):
        # cannot use self._look_back, because it is called before super().__init__
        return max(self.__required_history - look_back, 0)

    def __calculate_usable_samples_in_series(self, series):
        possible_chunks = max(series.shape[0] - self.__required_history - self.__look_ahead + 1, 0)
        return int(possible_chunks / self._chunk_size) * self._chunk_size

    def _get_usable_samples(self):
        return [self.__calculate_usable_samples_in_series(series)
                for series in self._data]

    def calc_item(self, idx):

        data_x = []
        data_y = []
        data_l = []

        self.__current_batch_indexes = self.__get_indexes_for_batch(idx)

        if self.__shuffle:  # we're stuck with slow mode
            for index in self.__current_batch_indexes:
                series, i = self.__index_to_index_in_series(index)
                x = self._data[series][i:(i + self._look_back + self._additionally_read), self._in_channels]
                y = self._data[series][i + self._look_back + self.__look_ahead - 1, self._out_channels]

                x = self.__insert_in_marks(series, i, x)
                y = self.__insert_out_marks(series, i, y)

                if self.__label_marks:
                    labels = self.__get_labels_marks(series, i)
                else:
                    labels = self._data[series][i + self._look_back + self.__look_ahead - 1, self._labels_channel]

                data_x.append(x)
                data_y.append(y)
                data_l.append(labels)
        else:  # read from memmap all data we will need at once
            unique_series = list(set([
                numpy.count_nonzero(self._series_offsets <= index) - 1 for index in self.__current_batch_indexes
            ]))
            ranges = []
            for series in unique_series:
                ranges.append((
                    max(self.__current_batch_indexes[0], self._series_offsets[series]) - self._series_offsets[
                        series] + self.__look_back_padding,
                    (min(self.__current_batch_indexes[-1] + 1, self._series_offsets[series + 1]) if
                     len(self._series_offsets) > series + 1 else self.__current_batch_indexes[-1] + 1) -
                    self._series_offsets[series] + self.__look_back_padding
                ))
            for series, (first, last) in zip(unique_series, ranges):
                xs = self._data[series][first:(last + self._look_back + self._additionally_read), self._in_channels]
                ys = self._data[series][
                     first + self._look_back + self.__look_ahead - 1:(last + self._look_back + self.__look_ahead - 1),
                     self._out_channels]

                if not self.__label_marks:
                    ls = self._data[series][
                         first + self._look_back + self.__look_ahead - 1:(
                                     last + self._look_back + self.__look_ahead - 1),
                         self._labels_channel]

                for i in range(0, last - first):
                    x = self.__insert_in_marks(series, first + i, xs[i:(i + self._look_back + self._additionally_read)])
                    y = self.__insert_out_marks(series, first + i, ys[i])

                    if self.__label_marks:
                        labels = self.__get_labels_marks(series, first + i)
                    else:
                        labels = ls[i]

                    data_x.append(x)
                    data_y.append(y)
                    data_l.append(labels)

        ndata_x = numpy.array(data_x, dtype=numpy.float32)
        ndata_y = numpy.array(data_y, dtype=numpy.float32)
        ndata_l = numpy.array(data_l, dtype=numpy.float32)
        if self._return_indexes:
            return ndata_x, ndata_y, ndata_l, [
                self.__index_to_index_in_series(index) for index in self.__current_batch_indexes
            ]
        else:
            return ndata_x, ndata_y, ndata_l

    def __insert_in_marks(self, series, i, x):
        if self.__in_marks is not False:
            marks = numpy.zeros((len(x), 1), dtype=bool)
            for mark_start, mark_stop in self.__anomaly_marks[series]:
                if mark_start <= i + self._look_back <= mark_stop:
                    marks[:, 0] = 1
            x = numpy.concatenate((x[:, :self.__in_marks], marks, x[:, self.__in_marks:]), axis=1)
        return x

    def __insert_out_marks(self, series, i, y):
        if self.__out_marks is not False:
            marks = numpy.zeros((1,), dtype=bool)
            for mark_start, mark_stop in self.__anomaly_marks[series]:
                if mark_start <= i + self._look_back + self.__look_ahead <= mark_stop:
                    marks[0] = 1
            y = numpy.concatenate((y[:self.__out_marks], marks, y[self.__out_marks:]), axis=0)
        return y

    def __get_labels_marks(self, series, i):
        marks = numpy.zeros((1,), dtype=bool)
        for mark_start, mark_stop in self.__anomaly_marks[series]:
            if mark_start <= i + self._look_back + self.__look_ahead <= mark_stop:
                marks[0] = 1
        return marks

    def __index_to_index_in_series(self, index):
        series = numpy.count_nonzero(self._series_offsets <= index) - 1
        sample = index - self._series_offsets[series]
        i = sample + self.__look_back_padding
        return series, i

    def __get_indexes_for_batch(self, idx):
        if self.__shuffle:
            if self.__use_random:
                # the probability of chunk spanning multiple series
                # when there is so much data that random is required is so low
                # that we will not even use computing power for checking it
                # and if it happens... well, no signal is noiseless, right?
                indexes = numpy.empty((int(self._batch_size / self._chunk_size), self._chunk_size), dtype=int)
                indexes[:, 0] = numpy.random.randint(self._total_usable_samples,
                                                     size=(int(self._batch_size / self._chunk_size),))
                for i in range(1, self._chunk_size):
                    indexes[:, i] = indexes[:, 0] + i
                return indexes.reshape((-1,))
            else:
                if self.__indexes is None:
                    self.__indexes = numpy.arange(self._total_usable_samples, step=self._chunk_size)
                    numpy.random.shuffle(self.__indexes)

                tmp = self.__indexes[idx * self._batch_size:(idx + 1) * self._batch_size]
                indexes = numpy.empty((tmp.shape[0], self._chunk_size), dtype=int)
                indexes[:, 0] = tmp
                for i in range(1, self._chunk_size):
                    indexes[:, i] = indexes[:, 0] + i
                return indexes.reshape((-1,))

        return numpy.arange(idx * self._batch_size,
                            min((idx + 1) * self._batch_size, self._total_usable_samples))

    @property
    def shape_x(self):
        # report "True" size, ignoring self.additionally_read
        return self._look_back, len(self._in_channels) + int(self.__in_marks is not False)

    @property
    def shape_y(self):
        return len(self._out_channels) + int(self.__out_marks is not False),

    @Reader.chunk_size.setter
    def chunk_size(self, value):
        Reader.chunk_size.fset(self, value)
        self.__indexes = None

    @property
    def current_out_indexes(self):
        indexes = []
        for index in self.__current_batch_indexes:
            series, i = self.__index_to_index_in_series(index)
            indexes.append((series, i + self._look_back + self.__look_ahead - 1))
        return indexes
