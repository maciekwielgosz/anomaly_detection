import logging

import numpy


class Reader(object):
    def __init__(self, name, data, in_channels, out_channels, labels_channel, look_back, batch_size, samples_percentage,
                 return_indexes, chunk_size, **kwargs):
        self._logger = logging.getLogger(__name__)
        self._name = name

        self._data = data
        self._in_channels = in_channels[:]  # copy
        self._out_channels = out_channels[:]  # copy
        self._labels_channel = labels_channel
        self._look_back = int(look_back)
        self._additionally_read = 0  # implemented for X2XSequence, use with care
        self._batch_size = int(batch_size)
        self._samples_percentage = samples_percentage
        self._return_indexes = return_indexes

        self._chunk_size = None
        self._usable_samples = None
        self._total_usable_samples = None
        self._len = None
        self._series_offsets = None

        self.chunk_size = chunk_size  # triggers validation

    def _get_usable_samples(self):
        """

        :return: Number of usable samples in dataset. Needs to honor chunk_size.
        """
        raise NotImplementedError()

    def calc_item(self, idx):
        """

        :param idx: int batch index
        :return: Calculated batch. Needs to honor chunk_size
        """
        raise NotImplementedError()

    def __len__(self):
        return self._len

    @property
    def batch_size(self):
        return self._batch_size

    @property
    def usable_samples(self):
        return self._usable_samples

    @property
    def total_samples(self):
        return self._total_usable_samples

    @property
    def dtype(self):
        return self._data[0].dtype

    @property
    def shape_x(self):
        # report "True" size, ignoring self.additionally_read
        return self._look_back, len(self._in_channels)

    @property
    def shape_y(self):
        return len(self._out_channels),

    @property
    def name(self):
        return self._name

    @property
    def additionally_read(self):
        return self._additionally_read

    @additionally_read.setter
    def additionally_read(self, value):
        self._logger.debug('Setting additionally read X samples to {}'.format(value))
        self._additionally_read = value

    @property
    def chunk_size(self):
        return self._chunk_size

    @chunk_size.setter
    def chunk_size(self, value):
        self._chunk_size = int(value)

        t, r = divmod(self._batch_size, self._chunk_size)
        if r != 0:
            new_batch_size = max(t, 1) * self._chunk_size
            self._logger.warning(
                'The batch size (={}) must be a multiple of the chunk size (={}). Forcing batch_size={}'.format(
                    self._batch_size, self._chunk_size, new_batch_size
                ))
            self._batch_size = new_batch_size

        self._usable_samples = self._get_usable_samples()
        self._total_usable_samples = int(numpy.sum(self._usable_samples))

        self._len = int(numpy.ceil(
            (self._total_usable_samples * self._samples_percentage) / float(self._batch_size))
        )

        self._series_offsets = numpy.cumsum(self._usable_samples) - self._usable_samples

    @property
    def current_out_indexes(self):
        return None
