import numpy

from analysta.data.sequences.readers.reader import Reader


class WindowsReader(Reader):
    """
    Do not use directly, use DataSequence instead
    """

    def __init__(self, **kwargs):
        super(WindowsReader, self).__init__(**kwargs)

        if self._look_back + self._chunk_size > self._data[0].shape[1]:
            msg = 'look_back + chunk_size cannot be larger than {}'.format(self._data[0].shape[1])
            self._logger.critical(msg)
            raise ValueError(msg)

        self._usable_chunks_in_sample = self.__calculate_usable_chunks_in_sample()

    def __calculate_usable_chunks_in_sample(self):
        possible_chunks = self._data[0].shape[1] - self._look_back
        return int(possible_chunks / self._chunk_size) * self._chunk_size

    def _get_usable_samples(self):
        usable_chunks = self.__calculate_usable_chunks_in_sample()
        return [series.shape[0] * usable_chunks for series in self._data]

    def calc_item(self, idx):
        indexes = [
            self.__index_to_index_in_series(index)
            for index in self.__get_indexes_for_batch(idx)
        ]

        data_x = []
        data_y = []
        data_l = []

        series_arr, samples_arr, chunks_arr = zip(*indexes)

        unique_series = list(set(series_arr))
        for series in unique_series:
            unique_samples = list(set([samples_arr[i] for i in range(len(samples_arr)) if series_arr[i] == series]))
            first_sample = min(unique_samples)
            last_sample = max(unique_samples)
            batch_data = self._data[series][first_sample:last_sample+1]
            for sample in unique_samples:
                chunks_in_sample = [
                    chunks_arr[i] for i in range(len(chunks_arr))
                    if series_arr[i] == series and samples_arr[i] == sample
                ]
                for cs in chunks_in_sample:
                    xs = batch_data[sample-first_sample, cs:cs+self._look_back+self._additionally_read][:, self._in_channels]
                    ys = batch_data[sample-first_sample, cs+self._look_back, self._out_channels]
                    ls = batch_data[sample-first_sample, cs+self._look_back, self._labels_channel]
                    data_x.append(xs)
                    data_y.append(ys)
                    data_l.append(ls)

        ndata_x = numpy.array(data_x, dtype=numpy.float32)
        ndata_y = numpy.array(data_y, dtype=numpy.float32)
        ndata_l = numpy.array(data_l, dtype=numpy.float32)

        if self._return_indexes:
            return ndata_x, ndata_y, ndata_l, indexes
        else:
            return ndata_x, ndata_y, ndata_l

    def __get_indexes_for_batch(self, idx):
        # TODO: pick random samples when samples_percentage < 1, not from beginning
        return numpy.arange(idx * self._batch_size,
                            min((idx + 1) * self._batch_size, self._total_usable_samples))

    def __index_to_index_in_series(self, index):
        series = numpy.count_nonzero(self._series_offsets <= index) - 1
        sample = int((index - self._series_offsets[series]) / self._usable_chunks_in_sample)
        chunk = index - sample * self._usable_chunks_in_sample
        return series, sample, chunk
