import numpy

from analysta.data.sequences.data_sequence import DataSequence


class X2XDataSequence(DataSequence):
    def __init__(self, *args, **kwargs):
        super(X2XDataSequence, self).__init__(*args, **kwargs)
        self._reader.additionally_read = 1

    def _postprocess_batch(self, batch_x, batch_y):
        batch_x, batch_y = super(X2XDataSequence, self)._postprocess_batch(batch_x, batch_y)
        return batch_x[:, :-1], batch_x[:, -1]

    def _get_shared_channels_indexes(self, in_channels, out_channels):
        return numpy.array(range(len(in_channels)))

    @property
    def shape_y(self):
        return self.shape_x
