import csv
import logging
import os
import math
import numpy
from sklearn.preprocessing import MinMaxScaler, OneHotEncoder


def convert_to_categories(data, edges, out_buckets):
    inds = convert_to_grid(data, edges)
    num_classes = None

    if edges[0] is not None:
        num_classes = len(edges[0]) + 1
    elif str(inds.dtype).find('int') > -1 and out_buckets > 0:
        num_classes = out_buckets

    if num_classes is not None:
        enc = OneHotEncoder(categories=[range(num_classes)], sparse=False)
        enc.fit(numpy.arange(0, num_classes).reshape((-1, 1)))
        categories = enc.transform(inds).astype(bool)
    else:
        categories = inds

    return categories


def convert_to_grid(data, edges):
    out = numpy.empty(data.shape, dtype=int)

    for i in range(data.shape[-1]):
        slice_index = tuple([slice(None)] * (data.ndim - 1) + [slice(i, i+1)])
        channel = data[slice_index]

        if edges[i] is not None:
            quantized = numpy.digitize(channel, edges[i])
        else:
            quantized = channel

        out[slice_index] = quantized

    return out


def channel_to_static_grid(_, grid):
    edges = numpy.arange(1.0/grid, 1.0, 1.0/grid)
    return edges


def channel_to_adaptive_grid(data_channel, grid):
    sorted_channel = numpy.sort(data_channel)
    total = sorted_channel.shape[0]
    bin_size = int(math.ceil(total / (grid + 0.0)))
    edges = [sorted_channel[i] for i in range(bin_size, total, bin_size)]
    return edges


def channel_to_recursive_adaptive_grid(data_channel, grid):
    def find_equal_histogram_candidates(data, still_needed):
        total = len(data)
        return [
            data[int(math.floor((total * (i + 1.0)) / (still_needed + 1.0)))]
            for i in range(still_needed)
        ]

    return channel_to_some_recursive_grid(data_channel, grid, find_equal_histogram_candidates)


def channel_to_cumulative_amplitude_grid(data_channel, grid):
    def find_cumulative_amplitude_candidates(data, still_needed):
        candidate_edges = []

        cumulative_amplitude = numpy.sum(numpy.abs(data))
        amplitudes_sum = 0
        index = 0

        for g in range(1, still_needed + 1):
            target_amplitude = (cumulative_amplitude * g) / still_needed if g != still_needed else cumulative_amplitude

            while amplitudes_sum < target_amplitude and index < len(data):
                amplitudes_sum += abs(data[index])
                index += 1

            candidate_edges.append(data[index - 1])

        return candidate_edges

    return channel_to_some_recursive_grid(data_channel, grid, find_cumulative_amplitude_candidates)


def channel_to_some_recursive_grid(data_channel, grid, find_candidates):
    sorted_channel = numpy.sort(data_channel)

    def find_edges(data, total_needed, found_edges):
        if not len(data):  # there is no more unique data left
            return found_edges

        still_needed = total_needed - len(found_edges)
        candidate_edges = found_edges + find_candidates(data, still_needed)

        unique_edges = list(set(candidate_edges))

        if len(unique_edges) == total_needed:
            return unique_edges

        duplicated_edges = {e for e in unique_edges if candidate_edges.count(e) > 1}
        found_edges = list(set(found_edges + list(duplicated_edges)))

        return find_edges(numpy.compress([
            n not in duplicated_edges for n in data
        ], data), total_needed, found_edges)

    edges = numpy.sort(find_edges(sorted_channel[1:-1], grid + 1, [sorted_channel[0], sorted_channel[-1]]))

    return edges[1:grid]


def get_edges(data_series, grid, algorithm, exclude=None):
    if isinstance(data_series, numpy.ndarray) and data_series.ndim == 2:
        data = data_series
    else:
        data = numpy.concatenate(data_series)

    exclude = exclude if exclude is not None else []
    edges = []

    for i in range(data.shape[1]):
        if i in exclude:
            edges.append(None)
            continue

        channel = data[:, i]

        channel_edges = None
        if algorithm == 'static':
            channel_edges = channel_to_static_grid(channel, grid)
        elif algorithm == 'adaptive':
            channel_edges = channel_to_adaptive_grid(channel, grid)
        elif algorithm == 'recursive_adaptive':
            channel_edges = channel_to_recursive_adaptive_grid(channel, grid)
        elif algorithm == 'cumulative_amplitude':
            channel_edges = channel_to_cumulative_amplitude_grid(channel, grid)
        elif algorithm == 'none':
            pass
        else:
            _logger = logging.getLogger(__name__)
            _logger.warning('Unknown grid algorithm "%s", using raw input instead' % (algorithm, ))

        edges.append(channel_edges)
    return edges


def normalize_data(data):
    scaler = MinMaxScaler()
    scaler.fit(data)
    normalized_data = scaler.transform(data)

    return normalized_data


def split_predictions_into_series(results_data, original_series_sizes, off):
    data_in_series = []
    s = 0
    for size in original_series_sizes:
        e = s + size - off
        data_in_series.append(results_data[s:e])
        s = e
    return data_in_series


def find_continuous_ones(channel):
    indexes = numpy.nonzero(numpy.diff(numpy.concatenate(([0], channel))))[0]

    if indexes.shape[0] % 2 == 1:
        indexes = numpy.append(indexes, [channel.shape[0]])
    indexes = indexes.reshape((-1, 2))
    return indexes


def get_mark_indexes(zoom, series_name, normalized_data, off=0, cut=None):
    if zoom is None:
        return numpy.array([])

    if zoom.isdigit():
        channel = normalized_data[:, int(zoom)]
        indexes = list(find_continuous_ones(channel))
    else:
        with open(zoom) as csv_file:
            reader = csv.DictReader(csv_file)
            indexes = []
            for row in reader:
                if os.path.splitext(os.path.basename(row['filename']))[0] == series_name:
                    start = int(row['start']) - off
                    end = min(int(row['stop']) - off, cut) if cut is not None else int(row['stop']) - off
                    # cannot use anything before offset and after cut
                    if start >= 0 and (cut is None or start < cut):
                        indexes.append((start, end))

    return numpy.array(indexes)


def unwrap_one_hot(output):
    axis = output.ndim - 1
    dtype = numpy.bool
    size = int(math.ceil(math.ceil(math.log2(output.shape[axis])) / 8))
    if size > 1:
        dtype = numpy.dtype('u{:d}'.format(size))
    return numpy.argmax(output, axis=axis).astype(dtype)
