import logging
import numpy as np

from analysta.analyzer.analyzer import Analyzer
from analysta.data.loading import load_series_from_paths, get_series_loader, get_anomaly_marks, get_marks_for_paths
from analysta.data.sequences import GridedDataSequence, DataSequence
from analysta.data.utils import unwrap_one_hot
from analysta.utils.config import Config, get_definition_from_path


def is_anomaly(x):
    return x.is_anomaly


def hook(f):
    def wrapper(*args):
        ret = f(*args)
        plugin_method = f.__name__[1:-5]
        for plugin in args[0].plugins:
            out = getattr(plugin, plugin_method)(*args[1:])
            if out is not None:
                args[0].stats.update(out)
        return ret

    return wrapper


class Detector(object):
    TRAIN = 'train'
    TEST = 'test'
    VAL = 'val'

    def __init__(self, config):
        if not isinstance(config, Config):
            config = Config(config)

        self._config = config
        self._logger = logging.getLogger(__name__)

        self.__raw_data = None
        self.__anomaly_marks = {}
        self.__dataset_mean = self.config.get('preparation.dataset_mean')
        self.__dataset_var = self.config.get('preparation.dataset_var')

        if self.__dataset_mean is not None:
            self.__dataset_mean = np.array(self.__dataset_mean)

        if self.__dataset_var is not None:
            self.__dataset_var = np.array(self.__dataset_var)

        self.__prepared_data = None
        self.__train_generator = None
        self.__val_generator = None
        self.__test_generator = None

        self.__model = None
        self.__analyzer = None

        self._stats = {}

        self.plugins = [
            self.__get_plugin(plugin) for plugin in self.config.get('setup.detector_plugins', [
              "analysta.detector.plugins.MetricsPlugin"  # MetricsPlugin is default
            ])
        ]

    # public methods
    def run(self, limit=None, fit_limit=None):
        self._stats = {}

        self._prepare_data_and_model()

        if self.config.get('analyzer.disabled', False):
            self.test_generator.sync_with(self.train_generator)
            for real_y, predicted_y in self.model.predict(self.test_generator, limit):
                if self.test_generator.use_one_hot:
                    real_y = self.test_generator.unwrap_one_hot(real_y)
                    predicted_y = self.test_generator.unwrap_one_hot(predicted_y)
                self._predicted_batch_available_hook(real_y, predicted_y)
            self._all_data_predicted_hook()
            return

        # force generators to return labels_channel - important for X2XDataSequence
        self.train_generator.return_labels = True
        self.val_generator.return_labels = True

        # set chunk size as required by analyzer
        self.train_generator.chunk_size = self.analyzer.window
        self.val_generator.chunk_size = self.analyzer.window

        # fit Analyzer on training data
        self._analyzer_available_hook()
        self.analyzer.fit(self.model.predict(self.train_generator, fit_limit))
        self._fitted_analyzer_available_hook()

        # ensure DataSequence necessary properties (e.g. dataset_mean or in_edges) are synced
        self.test_generator.sync_with(self.train_generator)

        # run Analyzer on testing data
        for result in self.analyzer.predict(self.model.predict(self.test_generator, limit)):
            real_y, predicted_y = result[:2]
            self._predicted_batch_available_hook(real_y, predicted_y)
            self._analyzed_batch_available_hook(*result)
        self._all_data_predicted_hook()
        self._all_data_analyzed_hook()

    # hooks
    @hook
    def _data_sequences_available_hook(self):
        training_batches = len(self.train_generator)
        testing_batches = len(self.test_generator)
        validation_batches = len(self.val_generator)
        look_back = self.config['preparation.look_back']
        required_history = self.config.get('preparation.required_history')
        if required_history is None:
            required_history = look_back
        else:
            required_history = max(required_history, look_back)
        self._logger.info('{} available for look_back={}, required_history={}'.format(
            self.train_generator.__class__.__name__,
            look_back,
            required_history,
        ))
        self._logger.info('{} training, {} validation and {} testing batches'.format(
            training_batches, validation_batches, testing_batches,
        ))

    @hook
    def _model_available_hook(self):
        self._stats['out.model.parameters'] = getattr(self.model, 'parameters', None)

        self._logger.info('Model available')

    @hook
    def _fitted_model_available_hook(self, history=None):
        self._logger.info('Fitted model available')

    @hook
    def _analyzer_available_hook(self):
        self._logger.info('Analyzer available')

    @hook
    def _fitted_analyzer_available_hook(self):
        self._logger.info('Fitted analyzer available')

    @hook
    def _predicted_batch_available_hook(self, real_y, predicted_y):
        pass

    @hook
    def _analyzed_batch_available_hook(self, real_y, predicted_y, real_samples_labels, predicted_samples_labels,
                                       prediction_scores, real_chunks_labels, predicted_chunk_labels):
        pass

    @hook
    def _all_data_predicted_hook(self):
        self._logger.info('All testing data predicted')

    @hook
    def _all_data_analyzed_hook(self):
        self._logger.info('All testing data analyzed')

    # privates
    def _fit_model(self):
        history = self.model.fit(self.train_generator, self.val_generator)
        return history

    def _prepare_data_and_model(self):
        self._data_sequences_available_hook()

        # reset stats
        self._stats = {}

        needs_training = True
        if self.config.get('model.load'):
            if self.config.get('preparation.edges_path'):
                self._logger.info('Loading model from {}...'.format(self.config['model.load']))
                model_cls = self.__get_model_cls()
                self.model = model_cls.load(
                    self.config['model.load'],
                    self.config, x_shape=self.x_shape,
                    y_shape=self.y_shape)
                needs_training = self.config.get('model.retraining', False)
            else:
                self._logger.critical('Cannot used saved model, preparation.edges_path not specified')
                exit(1)

        self._model_available_hook()

        if needs_training:
            history = self._fit_model()
            if hasattr(history, 'history'):
                for key in history.history.keys():
                    if key.startswith('val_'):
                        self._stats['out.model.val.' + key.replace('val_', '')] = history.history[key][-1]
                    else:
                        self._stats['out.model.train.' + key] = history.history[key][-1]
            self._stats['out.model.train.nb_epoch'] = self.model.total_epochs_run
            self._fitted_model_available_hook(history)

            self.__dataset_mean = self.train_generator.dataset_mean
            self.__dataset_var = self.train_generator.dataset_var

            self._stats['preparation.dataset_mean'] = self.__dataset_mean.tolist()
            self._stats['preparation.dataset_var'] = self.__dataset_var.tolist()
        else:
            self._fitted_model_available_hook()

    def __get_plugin(self, plugin):
        plugin_cls = get_definition_from_path(plugin, 'DetectorPlugin')
        return plugin_cls(self, self._logger)

    # getters & setters
    @property
    def config(self):
        return self._config

    @config.setter
    def config(self, value):
        self._config = value

    @property
    def raw_data(self):
        if self.__raw_data is None:
            self.__raw_data = self._load_raw_data()
        return self.__raw_data

    def _load_raw_data(self):
        train_paths = self.config.load_paths('data.train_paths')
        test_paths = self.config.load_paths('data.test_paths')
        val_paths = self.config.load_paths('data.val_paths')
        loader = get_series_loader(self.config)
        mmap_mode = self.config.setdefault('data.mmap_mode', 'r')

        raw_anomalies = get_anomaly_marks(self.config)

        return {
            'train': load_series_from_paths(train_paths, loader, mmap_mode)[0],
            'test': load_series_from_paths(test_paths, loader, mmap_mode)[0],
            'val': load_series_from_paths(val_paths, loader, mmap_mode)[0],
            'train_marks': get_marks_for_paths(raw_anomalies, train_paths),
            'test_marks': get_marks_for_paths(raw_anomalies, test_paths),
            'val_marks': get_marks_for_paths(raw_anomalies, val_paths)
        }

    @property
    def model(self):
        if self.__model is None:
            model_cls = self.__get_model_cls()
            self.__model = model_cls(
                self.config,
                x_shape=self.x_shape,
                y_shape=self.y_shape
            )
        return self.__model

    @property
    def y_shape(self):
        if self.train_generator is not None:
            return self.train_generator.shape_y
        if self.val_generator is not None:
            return self.val_generator.shape_y
        if self.test_generator is not None:
            return self.test_generator.shape_y

        out_buckets = self.config.get('preparation.out_buckets', 0)
        categories = self._config.get('data.categories', {})
        output_channels = self.config.get('data.output_channels', [])

        # TODO: figure out sth so this isn't needed - it duplicates the DataSequence job
        if out_buckets:
            return len(output_channels)*out_buckets,
        elif categories:
            return sum([
                len(categories[str(ch)].values())
                for ch in output_channels
            ]),
        else:
            return len(output_channels),

    @property
    def x_shape(self):
        if self.train_generator is not None:
            return self.train_generator.shape_x
        if self.val_generator is not None:
            return self.val_generator.shape_x
        if self.test_generator is not None:
            return self.test_generator.shape_x

        return self.config['preparation.look_back'], len(self.config['data.input_channels'])

    def __get_model_cls(self):
        model_cls = get_definition_from_path(self.config['setup.model'], 'Model')
        return model_cls

    @model.setter
    def model(self, value):
        self.__model = value

    @property
    def analyzer(self):
        if self.__analyzer is None:
            self.__analyzer = Analyzer(self._config, self.train_generator.use_one_hot)
        return self.__analyzer

    @property
    def edges(self):
        return {
            'in_edges': getattr(self.train_generator, 'in_edges', None),
            'out_edges': getattr(self.train_generator, 'out_edges', None)
        }

    @edges.setter
    def edges(self, value):
        if 'in_edges' in value:
            self.train_generator.in_edges = value['in_edges']
        if 'out_edges' in value:
            self.train_generator.out_edges = value['out_edges']

    @property
    def stats(self):
        return self._stats

    def __get_generator(self, set_key, samples_percentage=1, shuffle=True):
        in_buckets = self.config.get('preparation.out_buckets', 0)
        out_buckets = self.config.get('preparation.out_buckets', 0)

        model_cls = self.__get_model_cls()
        data_sequence_class = model_cls.data_sequence_class

        if data_sequence_class is None:
            if in_buckets or out_buckets:
                data_sequence_class = GridedDataSequence
            else:
                data_sequence_class = DataSequence

        return data_sequence_class(self.raw_data[set_key],
                                   self.config,
                                   samples_percentage=samples_percentage,
                                   allow_random=self.config.setdefault('preparation.allow_random_batch', False),
                                   shuffle=shuffle,
                                   name=set_key,
                                   dataset_mean=self.__dataset_mean,
                                   dataset_var=self.__dataset_var,
                                   anomaly_marks=self.raw_data[set_key+'_marks'])

    @property
    def train_generator(self):
        if self.__train_generator is None:
            sp = self.config.get('preparation.samples_percentage', 1)
            self.__train_generator = self.__get_generator('train', sp,
                                                          self.config.get('preparation.batch_shuffle', True))
        return self.__train_generator

    @property
    def val_generator(self):
        if self.__val_generator is None:
            sp = self.config.get('preparation.samples_percentage', 1)
            self.__val_generator = self.__get_generator('val', sp,
                                                        sp != 1)
        return self.__val_generator

    @property
    def test_generator(self):
        if self.__test_generator is None:
            sp = self.config.get('preparation.samples_percentage', 1)
            self.__test_generator = self.__get_generator('test', sp, sp != 1)
        return self.__test_generator
