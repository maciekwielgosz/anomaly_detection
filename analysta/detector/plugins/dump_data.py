import numpy
from analysta.detector.plugins import DetectorPlugin


class DumpDataPlugin(DetectorPlugin):
    def __init__(self, *args, **kwargs):
        super(DumpDataPlugin, self).__init__(*args, **kwargs)

        self.__data_batches = []  # will work for small datasets only, but whatever
        self.__predictions = []

    def predicted_batch_available(self, real_y, predicted_y):
        self.__data_batches.append(self._detector.test_generator.current_batch[0])
        self.__predictions.append(predicted_y)

    def all_data_predicted(self):
        data = numpy.squeeze(numpy.concatenate(self.__data_batches, axis=0))
        predictions = numpy.squeeze(numpy.concatenate(self.__predictions))
        print(data.shape)
        print(predictions.shape)

        numpy.savetxt('data_dump.csv', data, delimiter=',')
        numpy.savetxt('predictions_dump.csv', predictions, delimiter=',')
