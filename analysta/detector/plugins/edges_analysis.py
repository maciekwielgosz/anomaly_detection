import numpy

from analysta.data.utils import convert_to_grid
from .plugin import DetectorPlugin


class EdgesAnalysisPlugin(DetectorPlugin):
    def edges_available(self):
        in_buckets = self.__get_edges_cardinality('in_edges')
        out_buckets = self.__get_edges_cardinality('out_edges')

        self.__log_unique_bins('in_edges', 'in_buckets', in_buckets)
        self.__log_unique_bins('out_edges', 'out_buckets', out_buckets)
        self.__log_grid_utilization('train', 'in')
        self.__log_grid_utilization('train', 'out')
        self.__log_grid_utilization('test', 'in')
        self.__log_grid_utilization('test', 'out')

    def __get_edges_cardinality(self, key):
        return [len(c) + 1 if c is not None else None for c in self._detector.edges[key]]

    def __log_unique_bins(self, edges_key, grid_key, bins):
        unique = [len(numpy.unique(c)) + 1 if c is not None else None for c in self._detector.edges[edges_key]]

        self._logger.debug('{:s} unique bins: {:s}'.format(
            grid_key,
            str(["{}/{} ({:.2f}%)".format(u, g, u * 100.0 / g) for (u, g) in zip(unique, bins) if g is not None])
        ))

    def __log_grid_utilization(self, set_key, data_key):
        """
        Checks how many grid bins are actually used. Computationally intensive.
        Note: ignores required_history and look_ahead settings (calculates for all data).

        :param set_key: str 'train' or 'test'
        :param data_key: str 'in' or 'out'
        :return:
        """
        # TODO: convert to batch approach
        in_data = convert_to_grid(
            numpy.concatenate([
                s[:, self._detector.config.get('data.output_channels'.format(data_key))]
                for s in self._detector.raw_data[set_key]
            ], axis=0),
            self._detector.edges['{}_edges'.format(data_key)]
        )
        in_utilization = [
            len(numpy.unique(in_data[:, c]))
            for c in range(in_data.shape[1])
        ]
        self._logger.debug('{} {}_grid used bins: {:s}'.format(set_key, data_key, str(in_utilization)))
