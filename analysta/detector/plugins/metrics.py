import sklearn

from analysta.metrics import HUMANIZE_METRICS
from analysta.metrics.from_partials import *
from .plugin import DetectorPlugin


class MetricsPlugin(DetectorPlugin):
    def __init__(self, *args, **kwargs):
        super(MetricsPlugin, self).__init__(*args, **kwargs)

        self.__partials = {}
        self.__model_classes = None
        self.__output_channels = len(self._detector.config.get('data.output_channels', []))

        if self._detector.model.is_classifier:
            self.__model_classes = self._detector.config.get('preparation.out_buckets', 2)
            self.__partials['model'] = {
                # TODO: for now we only handle single output channel, figure out how to solve this for multi-channel output
                'confusion_matrix': numpy.zeros((self.__model_classes, self.__model_classes))
            }
        else:
            self.__partials['model'] = {
                'observed_samples': 0,
                'ss_tot': 0,  # total sum of squares, (y_real - y_mean)^2
                'ss_reg': 0,  # regression sum of squares (y_pred - y_mean)^2
                'ss_res': 0,  # residual sum of squares (y_real - y_pred)^2
                's_res': 0,  # sum of residuals |y_real - y_pred|

                # logarithmic residual sum of squares (log(y_real + 1) - log(y_pred - 1))^2
                'ss_log_res': 0,

                # sum of fractionals y_pred / y_real
                's_frac': 0,
            }

        self.__partials['analyzer.micro'] = {
            'confusion_matrix': numpy.zeros((2, 2))
        }

        self.__partials['analyzer.macro'] = {
            'confusion_matrix': numpy.zeros((2, 2))
        }

    def fitted_model_available(self, history=None):
        train_metrics = {}
        val_metrics = {}

        if history is not None:
            for key in history.history.keys():
                if key.startswith('val_'):
                    val_metrics[key.replace('val_', '')] = history.history[key][-1]
                else:
                    train_metrics[key] = history.history[key][-1]

            self.__display_metrics('Training', 'model', train_metrics)
            self.__display_metrics('Validation', 'model', val_metrics)

            return self.__convert_to_out('model', train=train_metrics, val=val_metrics)

    def predicted_batch_available(self, real_y, predicted_y):
        if not self._detector.model.is_classifier:  # regression
            axes = tuple(range(real_y.ndim - 1))

            # data is studentized, so the mean is 0
            self.__partials['model']['ss_tot'] += total_sum_of_squares(axes, real_y, predicted_y, 0)
            self.__partials['model']['ss_reg'] += regression_sum_of_squares(axes, real_y, predicted_y, 0)
            self.__partials['model']['ss_res'] += residual_sum_of_squares(axes, real_y, predicted_y, 0)
            self.__partials['model']['s_res'] += sum_of_residuals(axes, real_y, predicted_y, 0)
            self.__partials['model']['ss_log_res'] += log_residual_sum_of_squares(axes, real_y, predicted_y, 0)
            self.__partials['model']['s_frac'] += sum_of_fractionals(axes, real_y, predicted_y, 0)

            self.__partials['model']['observed_samples'] += numpy.multiply.reduce(
                numpy.array(real_y.shape)[..., axes], initial=None
            )
        else:  # classification
            real_reshaped = real_y.reshape((-1, self.__output_channels))
            predicted_reshaped = predicted_y.reshape((-1, self.__output_channels))
            # TODO: for now we only handle single output channel, figure out how to solve this for multi-channel output
            self.__partials['model']['confusion_matrix'] += sklearn.metrics.confusion_matrix(
                real_reshaped[:, 0],
                predicted_reshaped[:, 0],
                labels=list(range(self.__model_classes))
            )

    def analyzed_batch_available(self, real_y, predicted_y,
                                 real_samples_labels, predicted_samples_labels, prediction_scores,
                                 real_chunks_labels, predicted_chunks_labels):
        # analyzer.micro classification metrics
        self.__partials['analyzer.micro']['confusion_matrix'] += sklearn.metrics.confusion_matrix(
            real_samples_labels.reshape((-1, 1)),
            predicted_samples_labels.reshape((-1, 1)),
            labels=list(range(2))
        )

        if self._detector.test_generator.chunk_size > 1:
            # analyzer.macro classification metrics
            self.__partials['analyzer.macro']['confusion_matrix'] += sklearn.metrics.confusion_matrix(
                real_chunks_labels,
                predicted_chunks_labels,
                labels=list(range(2))
            )

    def all_data_predicted(self):
        out_metrics = {}

        # model metrics
        if not self._detector.model.is_classifier:  # regression
            model_metrics = regression_metrics(**self.__partials['model'])

            self.__display_metrics('Testing', 'model', model_metrics)
            out_metrics.update(self.__convert_to_out('model', test=model_metrics))
        else:  # classification
            self.__get_classification_metrics(out_metrics, 'model', self.__model_classes)

        return out_metrics

    def all_data_analyzed(self):
        out_metrics = {}

        # analyzer micro metrics
        self.__get_classification_metrics(out_metrics, 'analyzer.micro', 2)

        # analyzer macro metrics
        if self._detector.test_generator.chunk_size > 1:
            self.__get_classification_metrics(out_metrics, 'analyzer.macro', 2)

        return out_metrics

    def __get_classification_metrics(self, out_metrics, obj, num_classes):
        metrics = classification_metrics(
            self.__partials[obj]['confusion_matrix'],
            average='balanced' if num_classes > 2 else 'binary'
        )

        # cannot compute binary metrics (APS, ROC curve, PR curve) with currently gathered partials
        # if num_classes == 2:
        #     metrics.update(binary_classification_metrics())

        self.__display_metrics('Testing', obj, metrics)
        out_metrics.update(self.__convert_to_out(obj, test=metrics))

    def __display_metrics(self, dataset, obj, metrics):
        msg = '{} set {} metrics:'.format(dataset, obj)
        if len(metrics):
            for key, value in metrics.items():
                if key in HUMANIZE_METRICS:
                    key = HUMANIZE_METRICS[key]['long']
                else:
                    key = key.upper()
                try:
                    msg += '\n\t{}: {:.4f}'.format(key, float(value))
                except TypeError:
                    msg += '\n\t{}: {}'.format(key, str(value))
        else:
            msg += 'None'
        self._logger.info(msg)

    @staticmethod
    def __convert_to_out(obj, **kwargs):
        out_metrics = {}
        for dataset, metrics in kwargs.items():
            for key, value in metrics.items():
                out_metrics['out.{}.{}.{}'.format(obj, dataset, key)] = value
        return out_metrics
