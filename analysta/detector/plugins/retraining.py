import keras
import numpy

from analysta.detector.plugins.plugin import DetectorPlugin


class WeightsMaskCallback(keras.callbacks.Callback):
    def __init__(self):
        super(WeightsMaskCallback, self).__init__()

        self.__mask = []

    def on_train_begin(self, logs=None):
        self.__mask = []

        for layer in self.model.get_weights():
            self.__mask.append(layer != 0)

    def on_batch_end(self, batch, logs=None):
        masked_weights = self.model.get_weights()

        for idx in range(len(masked_weights)):
            masked_weights[idx] *= self.__mask[idx]

        self.model.set_weights(masked_weights)


class RetrainingPlugin(DetectorPlugin):
    def model_available(self):
        # recompile the model - SGD is better for retraining, also we need smaller learning rate
        optimizer = keras.optimizers.SGD(lr=0.001, momentum=0.0, decay=0.0, nesterov=False)
        wrapper = self._detector.model
        wrapper.model.compile(optimizer=optimizer, loss=wrapper.loss, metrics=wrapper.metrics)

        wrapper.extra_callbacks = [
            WeightsMaskCallback()
        ]
