class DummyHistory(object):
    def __init__(self, **kwargs):
        self.history = {key: [value] if not isinstance(value, list) else value for key, value in kwargs.items()}
