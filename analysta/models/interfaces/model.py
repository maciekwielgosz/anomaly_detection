import logging
import sys

import numpy
from sklearn.metrics import accuracy_score
from tqdm import tqdm

from analysta.models.dummy_history import DummyHistory


class Model(object):
    data_sequence_class = None

    def __init__(self, config, x_shape, y_shape, model=None):
        self._config = config
        self._logger = logging.getLogger(__name__)

        self._x_shape = x_shape
        self._y_shape = y_shape

        # TODO: handle it correctly, we can have multi-channel regression after all...
        self._is_classifier = len(self._y_shape) > 1

        self._verify_data_compatibility()
        self.__model = model

    def _verify_data_compatibility(self):
        if self.uses_single_out_channel and len(self._config.get('data.output_channels', [])) != 1:
            self._logger.critical('Only a single output channel is supported for {}'.format(self.__class__.__name__))
            exit(1)

    def _create_model(self):
        raise NotImplementedError()

    def fit(self, train_generator, val_generator):
        self._set_dataset_mean_and_var(train_generator, val_generator)
        x_train, y_train = self._get_concatenated_train_set(train_generator)

        self.model.fit(x_train, y_train)

        val_acc = []
        for y_val, y_pred in self.predict(val_generator):
            val_acc.append(accuracy_score(y_val, y_pred))

        return DummyHistory(val_acc=val_acc)

    def _get_concatenated_train_set(self, train_generator):
        x_train, y_train = zip(*train_generator[:])

        return numpy.concatenate(x_train), numpy.concatenate(y_train)

    def predict(self, data_generator, limit=None):
        if limit is None or limit > len(data_generator):
            limit = len(data_generator)

        with tqdm(total=limit, file=sys.stdout) as pbar:
            for batch_x, batch_y in data_generator[:limit]:
                pred = self._predict_on_batch(batch_x)
                yield batch_y, pred
                pbar.update(1)

    def _predict_on_batch(self, x):
        return self.model.predict(x)

    def cleanup(self):
        pass

    @staticmethod
    def _set_dataset_mean_and_var(train_generator, val_generator, force_eval=False):
        if train_generator.dataset_mean is None or train_generator.dataset_var is None:
            train_generator.reset_normalize()
        if val_generator.dataset_mean is None or val_generator.dataset_var is None:
            val_generator.sync_with(train_generator)

        if force_eval:
            _ = [None for _ in train_generator[:]]  # force train set evaluation

    @property
    def total_epochs_run(self):
        return 1

    def save(self, filepath):
        self._logger.warning('Saving %s is not supported.' % (self.__class__.__name__, ))
        return False

    @staticmethod
    def load(filepath, config, x_shape, y_shape):
        return None

    @property
    def model(self):
        if self.__model is None:
            self.__model = self._create_model()
        return self.__model

    @property
    def is_classifier(self):
        return self._is_classifier

    @property
    def uses_single_out_channel(self):
        return True
