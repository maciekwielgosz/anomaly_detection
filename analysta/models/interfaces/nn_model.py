import logging
import warnings

from analysta.data.sequences.data_sequence import DataSequence
from analysta.models.interfaces.model import Model
from analysta.utils import get_definition_from_path, get_results_path
from analysta.utils.performance.tf_multi_gpu import tf, multi_gpu_model, get_available_gpus


class NNModel(Model):
    """
    Base class for Keras-based neural network models.
    Runs on multi GPU if possible.
    """

    def __init__(self, config, x_shape, y_shape, model=None, template_model=None):
        self.__batch_size = config['preparation.batch_size']
        self.__max_nb_epoch = config.setdefault('model.max_nb_epoch', 100)
        self.__total_nb_epoch_run = 0
        self.__gpus = config.setdefault('model.gpus', get_available_gpus())

        self.__early_stopping = config.setdefault('model.early_stopping', False)
        if self.__early_stopping is True:
            self.__early_stopping = 2  # default patience if not specified

        self.__save_best_checkpoint = config.setdefault('model.save_best_checkpoint', True)
        self.__template_model = template_model if template_model is not None else model

        self._model_optimizer = config.setdefault('model.optimizer', 'adam')

        # TODO: correctly handle y_shape
        if len(y_shape) < 2:  # regression
            self._model_loss = config.setdefault('model.loss', 'mean_squared_error')
            self._model_metrics = config.setdefault('model.metrics', ['mean_squared_error'])
        else:
            self._model_loss = config.setdefault('model.loss', 'categorical_crossentropy')
            self._model_metrics = config.setdefault('model.metrics', ['accuracy'])

        if model is None and template_model is not None:
            model = self._get_compiled_model()

        super(NNModel, self).__init__(config, x_shape, y_shape, model)

        if self.__template_model is not None:
            self.__log_model_summary()

        self.__checkpoint_path = None

        self.__extra_callbacks = []

    def fit(self, train_generator, val_generator):
        from keras.callbacks import EarlyStopping, ModelCheckpoint

        if self.__total_nb_epoch_run == 0:
            # for whatever reason keras is running validation generator
            # BEFORE train generator, and then normally after the epoch
            # we need to force_eval for dataset_mean / dataset_var to be correct
            self._set_dataset_mean_and_var(train_generator, val_generator, force_eval=True)

        verbosity = 0
        logging_level = self._logger.getEffectiveLevel()
        if logging_level == logging.INFO:
            verbosity = 2
        elif logging_level == logging.DEBUG:
            verbosity = 1

        if 'accuracy' in self._model_metrics:
            metric_key = 'acc'
        else:
            metric_key = self._model_metrics[0]

        callbacks = []
        if self.__early_stopping:
            callbacks.append(EarlyStopping(monitor='val_' + metric_key,
                                           verbose=min(verbosity, 1),
                                           patience=self.__early_stopping,
                                           restore_best_weights=True))
        elif self.__save_best_checkpoint:
            self.__checkpoint_path = get_results_path('checkpoint', 'h5')
            callbacks.append(ModelCheckpoint(self.__checkpoint_path,
                                             verbose=min(verbosity, 1),
                                             monitor='val_' + metric_key,
                                             save_weights_only=True,
                                             save_best_only=True))

        callbacks.extend(self.extra_callbacks)

        if len(callbacks) == 0:
            callbacks = None

        if self.__max_nb_epoch > self.__total_nb_epoch_run:
            history = self.model.fit_generator(generator=train_generator, validation_data=val_generator,
                                               epochs=self.__max_nb_epoch, verbose=verbosity,
                                               initial_epoch=self.__total_nb_epoch_run, callbacks=callbacks)

            self.__total_nb_epoch_run += len(history.history[metric_key])

            # restore best model
            if self.__checkpoint_path is not None:
                self.model.load_weights(self.__checkpoint_path)

            return history
        else:
            return None

    def _predict_on_batch(self, x):
        return self.model.predict_on_batch(x)

    @Model.total_epochs_run.getter
    def total_epochs_run(self):
        return self.__total_nb_epoch_run

    def save(self, filepath):
        self.__template_model.save(filepath)
        return True

    @staticmethod
    def load(filepath, config, x_shape, y_shape):
        from keras.models import load_model

        with tf.device("/cpu:0"):
            with warnings.catch_warnings():
                warnings.simplefilter("ignore")
                template_model = load_model(filepath)

        tmp_cells = [l.get_config().get('units') for l in template_model.layers]
        cells = tuple([c for c in tmp_cells if c is not None])

        # detect config/model mismatch
        if tuple(config['model.cells']) != cells[:-1]:
            logging.getLogger(__name__).critical("""Loaded model and provided config do not match!
            Cells in model: %s
            Cells in config: %s""" % (cells[:-1], config['model.cells']))

        model_cls = get_definition_from_path(config['setup.model'], 'Model')

        return model_cls(config, x_shape, y_shape, None, template_model)

    @property
    def max_epochs(self):
        return self.__max_nb_epoch

    @max_epochs.setter
    def max_epochs(self, nb_epoch):
        if self.__total_nb_epoch_run >= nb_epoch:
            raise ValueError('Model was already trained for %i epochs, cannot change to %i' % (
                self.__total_nb_epoch_run, nb_epoch))
        self.__max_nb_epoch = nb_epoch

    @property
    def loss(self):
        return self._model_loss

    @property
    def metrics(self):
        return self._model_metrics

    @property
    def template_model(self):
        if self.__template_model is None:
            getattr(self.model, 'force_eval', None)
        return self.__template_model

    @property
    def parameters(self):
        return self.template_model.count_params()

    def _create_template_model(self):
        raise NotImplementedError

    def _create_model(self):
        with tf.device("/cpu:0"):
            self.__template_model = self._create_template_model()

        self.__log_model_summary()

        return self._get_compiled_model()

    def __log_model_summary(self):
        if self._logger.getEffectiveLevel() == logging.DEBUG:
            self._logger.debug('Model summary:\n')
            self.__template_model.summary()

    def _get_compiled_model(self):
        model = multi_gpu_model(self.__template_model, gpus=self.__gpus)
        model.compile(loss=self._model_loss, optimizer=self._model_optimizer, metrics=self._model_metrics)

        return model

    def cleanup(self):
        pass
        # from keras.backend import clear_session
        # clear_session()

    @property
    def uses_single_out_channel(self):
        return False

    @property
    def extra_callbacks(self):
        return self.__extra_callbacks

    @extra_callbacks.setter
    def extra_callbacks(self, callbacks):
        from keras.callbacks import EarlyStopping, ModelCheckpoint

        filtered = []

        for callback in callbacks:
            if isinstance(callback, (EarlyStopping, ModelCheckpoint)):
                self._logger.warn('{} wll be automatically created, skipping'.format(callback.__class__.__name__))
            else:
                filtered.append(callback)

        self.__extra_callbacks = filtered
