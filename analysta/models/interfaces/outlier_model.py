import sys

import numpy
from tqdm import tqdm

from analysta.data.sequences import OutliersFlatDataSequence
from analysta.models.interfaces.model import Model


class OutlierModel(Model):
    data_sequence_class = OutliersFlatDataSequence

    @staticmethod
    def __calculate_contamination(y):
        outliers = numpy.count_nonzero(y < 0)
        if outliers:
            return float(outliers) / float(y.shape[0])
        else:
            return 0.00001

    def _get_concatenated_train_set(self, train_generator):
        x_train, y_train = super(OutlierModel, self)._get_concatenated_train_set(train_generator)

        self._contamination = self.__calculate_contamination(y_train)

        return x_train, y_train

    def predict(self, data_generator, limit=None):
        if limit is None or limit > len(data_generator):
            limit = len(data_generator)

        with tqdm(total=limit, file=sys.stdout) as pbar:
            for batch_x, batch_y in data_generator[:limit]:
                pred = self._predict_on_batch(batch_x)
                yield data_generator.inverse_mapping(batch_y), data_generator.inverse_mapping(pred)
                pbar.update(1)

    @property
    def is_classifier(self):
        return True
