import logging

import numpy

from analysta.models.interfaces.x2x_model import X2XModel


class UnivariateModel(X2XModel):
    """
    Creates several underlying model instances to handle several input channels
    while algorithm only allows one.
    """

    def _create_model(self):
        models = []
        for ch in range(self._x_shape[-1]):
            models.append(self._create_model_for_channel(ch))
        return models

    def _predict_on_batch(self, x):
        predictions = []
        for sample in x:
            predictions.append(numpy.vstack([
                self.model[ch].predict(sample[..., ch])[-len(sample):]
                for ch in range(sample.shape[1])
            ]).T)
        return numpy.array(predictions)[:, -1, :]

    def _create_model_for_channel(self, channel_index):
        raise NotImplementedError()

    def _verify_param_compatibility(self, param, param_name):
        if param.shape[0] != self._x_shape[-1]:
            logging.getLogger(__name__).critical('{} length (={}) does not match the number of channels (={}).'.format(
                param_name,
                param.shape[0],
                self._x_shape[-1]
            ))
            exit(1)
