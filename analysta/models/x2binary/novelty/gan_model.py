from analysta.algorithms.gan import GAN
from analysta.algorithms.gan_rnn.gan import GANRNN
from analysta.data.sequences import DataSequence
from analysta.models.dummy_history import DummyHistory
from analysta.models.interfaces.model import Model


class GANModel(Model):
    def __init__(self, *args, **kwargs):
        super(GANModel, self).__init__(*args, **kwargs)

        if self._y_shape[-1] == 0:
            self._data_sequence_class = DataSequence

        self.__max_nb_epoch = self._config.setdefault('model.max_nb_epoch', 100)
        self.__gpus = self._config.setdefault('model.gpus', None)

    def _create_model(self):
        try:
            return GANRNN(
                max_nb_epoch=self.__max_nb_epoch,
                x_shape=self._x_shape,
                gpus=self.__gpus
            )
        except ValueError as e:
            self._logger.critical(str(e))
            exit(1)

    def fit(self, train_generator, val_generator):
        train_generator.setup_one_hot(False)
        val_generator.setup_one_hot(False)

        self._set_dataset_mean_and_var(train_generator, val_generator)
        if self._config.get('model.retraining'):
            history = self.model.fit_discriminator(train_generator=train_generator, val_generator=val_generator)
        else:
            history = self.model.fit_generator(train_generator=train_generator, val_generator=val_generator)
        return DummyHistory(**history)

    def predict(self, data_generator, limit=None):
        data_generator.setup_one_hot(False)
        return super(GANModel, self).predict(data_generator, limit)

    def _predict_on_batch(self, x):
        return self.model.predict_on_batch(x)

    def save(self, filepath):
        print('saving pytorch model')
        self.model.save(filepath=filepath)
        return True

    @staticmethod
    def load(filepath, config, x_shape, y_shape):
        print('loading pytorch model')
        model = GAN(
            max_nb_epoch=config.setdefault('model.max_nb_epoch', 100),
            x_shape=x_shape,
            gpus=config.setdefault('model.gpus', None)
        )
        model.load(filepath=filepath)
        return GANModel(config,x_shape=x_shape, y_shape=y_shape, model=model)