from analysta.models.interfaces.outlier_model import OutlierModel
from sklearn.ensemble import IsolationForest


class IsolationForestModel(OutlierModel):
    def _create_model(self):
        return IsolationForest(contamination=self._contamination, verbose=True, n_jobs=-1, behaviour='new')
