import numpy

from analysta.algorithms.moving_average import MovingAverage
from analysta.models.interfaces.univariate_model import UnivariateModel


class MovingAverageModel(UnivariateModel):
    def __init__(self, config, x_shape, y_shape, model=None):
        super(MovingAverageModel, self).__init__(config, x_shape, y_shape, model)

        self.__look_back = config['preparation.look_back']

        self.__window = numpy.array(config.setdefault('model.window_size', [self.__look_back] * x_shape[-1]))
        self._verify_param_compatibility(self.__window, 'model.window_size')

    def _create_model_for_channel(self, channel_index):
        return MovingAverage(self.__window[channel_index])
