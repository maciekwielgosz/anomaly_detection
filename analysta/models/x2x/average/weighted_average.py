import numpy

from analysta.algorithms.weighted_average import WeightedAverage
from analysta.models.interfaces.univariate_model import UnivariateModel


class WeightedAverageModel(UnivariateModel):
    def __init__(self, config, x_shape, y_shape, model=None):
        super(WeightedAverageModel, self).__init__(config, x_shape, y_shape, model)

        self.__look_back = config['preparation.look_back']

        self.__weights = numpy.array(config.setdefault('model.weights',
                                                       [self.__fibonacci(self.__look_back)] * x_shape[-1]))
        self._verify_param_compatibility(self.__weights, 'model.weights')

    def _create_model_for_channel(self, channel_index):
        return WeightedAverage(self.__weights[channel_index])

    @staticmethod
    def __fibonacci(limit):
        n = numpy.arange(1, limit)
        sqrt5 = numpy.sqrt(5)
        phi = (1 + sqrt5) / 2
        return numpy.rint((phi ** n - (-1 / phi) ** n) / sqrt5)
