import numpy

from analysta.algorithms.exponential_smoothing import ExponentialSmoothing
from analysta.models.interfaces.univariate_model import UnivariateModel


class ExponentialSmoothingModel(UnivariateModel):
    def __init__(self, config, x_shape, y_shape, model=None):
        super(ExponentialSmoothingModel, self).__init__(config, x_shape, y_shape, model)

        self.__alpha = numpy.array(config.setdefault('model.alpha', [0.9] * x_shape[1]))
        self._verify_param_compatibility(self.__alpha, 'model.alpha')

    def _create_model_for_channel(self, channel_index):
        return ExponentialSmoothing(self.__alpha[channel_index])
