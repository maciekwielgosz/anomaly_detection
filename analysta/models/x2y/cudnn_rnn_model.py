from analysta.models.x2y.rnn_model import RNNModel
from analysta.utils import get_definition_from_path
from analysta.utils.performance.tf_multi_gpu import get_available_gpus


class CuDNNRNNModel(RNNModel):
    @RNNModel.rnn_class.getter
    def rnn_class(self):
        """
        RNN-type Layer to be use in model. By default keras.layers.CuDNNLSTM
        :return:
        """
        if get_available_gpus() > 0:
            return get_definition_from_path(self._config.setdefault('model.rnn_class', 'keras.layers.CuDNNLSTM'), 'RNNClass')
        else:
            raise TypeError("CuDNN models are not available on this machine.")
