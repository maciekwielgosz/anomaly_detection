import logging

import numpy
from skopt.space import Real, Integer, Categorical


def get_optimization_space(config, optimizer):
    dimensions = []
    for key, setup in optimizer.get('optimize').items():
        bounds_min = float(setup.get('min', '-Inf'))
        bounds_max = float(setup.get('max', 'Inf'))
        categories = setup.get('categories')

        dimension_type = setup.get('type', 'categorical' if categories is not None else 'real')
        dimension_prior = setup.get('prior', None if dimension_type == 'categorical' else 'uniform')

        if 'array_like' in setup or 'array_shape' in setup:
            if 'array_like' in setup:
                shape = numpy.array(config[setup['array_like']]).shape
            else:
                shape = setup['array_shape']
            if len(shape) > 2:
                logging.getLogger(__name__).critical(
                    'Array parameters must have at most two dimensions. {} has {}.'.format(key, len(shape))
                )
                exit(1)
            if len(shape) == 1:
                indices = [str(i) for i in range(shape[0])]
            else:
                indices = [[str(i) + ',' + str(j) for j in range(shape[1])] for i in range(shape[0])]

            for i in indices:
                dimensions.append(
                    create_dimension(key + '.' + i, dimension_type, bounds_max, bounds_min, categories, dimension_prior)
                )
        else:
            dimensions.append(
                create_dimension(key, dimension_type, bounds_max, bounds_min, categories, dimension_prior))
    return dimensions


def create_dimension(name, dimension_type, bounds_max, bounds_min, categories, dimension_prior):
    dimension = None
    if dimension_type == 'real':
        dimension = Real(bounds_min, bounds_max, prior=dimension_prior, name=name)
    elif dimension_type == 'integer':
        dimension = Integer(bounds_min, bounds_max, name=name)
    elif dimension_type == 'categorical':
        dimension = Categorical(categories, prior=dimension_prior, name=name, transform='identity')
    else:
        logging.getLogger(__name__).critical('Unknown dimension type: {}'.format(dimension_type))
        exit(1)

    return dimension


def update_config_with_space_kwargs(config, params):
    last_key = None
    for key, value in params.items():
        split_key = key.split('.')
        last_key_part = split_key[-1]
        if last_key_part[0].isdigit():
            key_root = '.'.join(split_key[:-1])
            if key_root != last_key:
                config[key_root] = []
                last_key = key_root
            values_array = config[key_root]
            if ',' in last_key_part:
                row = last_key_part.split(',')[0]
                if int(row) < len(values_array):
                    values_array.append([])
                    values_array = values_array[-1]
                else:
                    values_array = values_array[int(row)]
            values_array.append(value.item() if hasattr(value, 'item') else value)
        else:
            config[key] = value.item() if hasattr(value, 'item') else value  # convert from NumPy to Python type
            last_key = key
