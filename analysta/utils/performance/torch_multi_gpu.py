import torch


class AutoTorch(object):
    def __init__(self, gpus=None):
        self.__device = None
        self.__no_of_GPUs = gpus

    def __enter__(self):
        if torch.cuda.is_available() and self.__no_of_GPUs != 0:
            self.__device = torch.device("cuda:0")
            self.__no_of_GPUs = min(torch.cuda.device_count(),
                                    self.__no_of_GPUs if self.__no_of_GPUs is not None else int(float('Inf')))
        else:
            self.__device = torch.device("cpu")

        return self

    def __exit__(self, *args):
        return False

    @property
    def device(self):
        return self.__device

    @property
    def no_of_GPUs(self):
        return self.__no_of_GPUs

    def DataParallel(self, model):
        if self.__no_of_GPUs > 1:
            return torch.nn.DataParallel(model, list(range(self.__no_of_GPUs)))
        else:
            return model

