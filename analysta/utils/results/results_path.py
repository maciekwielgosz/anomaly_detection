import os
import uuid


def get_results_path(prefix, ext='csv', subdir_name=None, subdir=True):
    file_name = prefix + '-' + str(uuid.uuid1()) + '.' + ext

    if subdir:
        subdir_name = subdir_name if subdir_name is not None else 'results'
        file_path = os.path.join(subdir_name, file_name)
    else:
        file_path = file_name

    return file_path
