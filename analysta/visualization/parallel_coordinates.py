import matplotlib.pyplot as plt
import matplotlib.cm
import numpy
from matplotlib.lines import Line2D
from pandas import read_csv

matplotlib.rcParams['font.family'] = 'serif'


def linestyle_generator():
    styles = [
        'solid',
        # 'dotted',
        'dashed',
        'dashdot'
    ]
    i = 0
    while True:
        yield styles[i]
        i = i + 1 if i < len(styles) - 1 else 0


def colormap_generator():
    maps = ['Blues', 'Reds', 'Greens', 'Greys', 'Purples', 'Oranges', 'BuPu', 'Wistia', 'YlGn', 'OrRd']
    i = 0
    while True:
        yield maps[i]
        i = i + 1 if i < len(maps) - 1 else 0


def parallel_coordinates(filename, to_plot, group_by, label_lambda, directions=None, color_key=None, sort_key=None,
                         scale=None, transforms=None, normalize=True, revert_color=False, sync_scale=None,
                         linestyle=None, texts=None, ylabel=None):
    df_min, df_max, df_grouped, directions, scale, x, color_key = setup_parallel_coordinates(filename, group_by,
                                                                                             to_plot, transforms,
                                                                                             directions, scale,
                                                                                             normalize, sort_key,
                                                                                             color_key,
                                                                                             sync_scale=sync_scale)

    lines = []
    names = []
    styles = linestyle_generator()
    maps = colormap_generator()

    if linestyle == 'group':
        line_map = [[styles.next()]*len(group) for _, group in df_grouped]
        legend_line_map = [item[0] for item in line_map]
    else:
        line_map = [['solid']*len(group) for _, group in df_grouped]
        legend_line_map = ['solid'] * len(df_grouped)

    for g, (index, group) in enumerate(df_grouped):
        if color_key:
            colors = group[color_key].tolist()
        else:
            colors = [0.75] * len(group)
        cmap = matplotlib.cm.get_cmap(maps.next())
        color_lines = []
        for rowindex, row in enumerate(group[to_plot].itertuples(index=False)):
            color_val = 1 - colors[rowindex] if revert_color else colors[rowindex]
            color_val = color_val * 0.8 + 0.2  # prevent near-white from being used
            line, = plt.plot(x, row,
                             color=cmap(color_val),
                             linestyle=line_map[g][rowindex])
            color_lines.append(line)
        line_copy = Line2D((0, 1), (0, 0))
        line_copy.update_from(color_lines[int(len(color_lines)/2)])
        line_copy.set_linestyle(legend_line_map[g])
        line_copy.set_linewidth(2)
        lines.append(line_copy)
        append_series_name(group_by, index, names)

    annotate(df_min, df_max, directions, label_lambda, lines, names, scale, to_plot, x, normalize, texts, ylabel)


def parallel_coordinates_mean(filename, to_plot, group_by, label_lambda, directions=None, scale=None, transforms=None,
                              normalize=True, texts=None, ylabel=None):
    df_min, df_max, df_grouped, directions, scale, x, _ = setup_parallel_coordinates(filename, group_by, to_plot,
                                                                                     transforms, directions, scale,
                                                                                     normalize)

    lines = []
    names = []
    maps = colormap_generator()

    for g, (index, group) in enumerate(df_grouped):
        cmap = matplotlib.cm.get_cmap(maps.next())
        color = cmap(0.75)

        plt.fill_between(x, group[to_plot].min(), group[to_plot].max(), alpha=0.25, linewidth=0, color=color)
        line, = plt.plot(x, group[to_plot].mean(), color=color, linewidth=2)

        lines.append(line)
        append_series_name(group_by, index, names)

    annotate(df_min, df_max, directions, label_lambda, lines, names, scale, to_plot, x, normalize, texts, ylabel)


def append_series_name(group_by, index, names):
    if isinstance(index, tuple):
        names.append({k: index[i] for i, k in enumerate(group_by)})
    else:
        names.append({group_by[0]: index})


def annotate(df_min, df_max, directions, label_lambda, lines, names, scale, to_plot, x, normalize=True, texts=None,
             ylabel=None):
    if texts is None:
        texts = dict(zip(to_plot, to_plot))

    ax = plt.gca()

    if label_lambda:
        ax.legend(lines, map(label_lambda, names), fontsize=12, loc='lower left')

    ax.tick_params(axis='x', pad=18)
    ax.set_xticks(x)
    ax.set_xticklabels([texts[l] for l in to_plot], family='serif')

    if ylabel is not None:
        plt.gca().set_ylabel(ylabel, family='serif')

    if normalize:
        ax.set_yticks([])

    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.spines['bottom'].set_visible(False)
    ax.spines['left'].set_visible(False)

    for pos_x, v, d, s in zip(x, to_plot, directions, scale):
        max_v = df_max[v] if normalize else 1
        min_v = df_min[v] if normalize else 0
        if normalize:
            max_s = '%.4f' % (max_v, ) if max_v != int(max_v) else '%i' % (max_v, )
            min_s = '%.4f' % (min_v,) if min_v != int(min_v) else '%i' % (min_v,)
            plt.gca().annotate(
                min_s if d else max_s,
                (pos_x, 0), (0, -13), textcoords='offset points', fontsize=12, horizontalalignment='center')
            plt.gca().annotate(
                max_s if d else min_s,
                (pos_x, 1), (0, 6), textcoords='offset points', fontsize=12, horizontalalignment='center')
        if s:
            plt.gca().annotate(
                '%s scale' % (s,),
                (pos_x, 1), (0, 20), textcoords='offset points', fontsize=8, horizontalalignment='center',
                fontstyle='italic')


def setup_parallel_coordinates(filename, group_by, to_plot, transforms=None, directions=None, scale=None,
                               normalize=True, sort_key=None, color_key=None, sync_scale=None):

    if sort_key is None:
        sort_key = color_key
    if color_key is None and sort_key is not None:
        if isinstance(sort_key, list) or isinstance(sort_key, tuple):
            color_key = sort_key[0]
        else:
            color_key = sort_key

    df_min, df_max, df_grouped, directions, scale = read_and_transform_data(filename, group_by, to_plot, transforms, directions,
                                                                scale, normalize, color_key, sort_key, sync_scale)

    x = numpy.arange(len(to_plot))

    plt.figure()

    for v in x:
        plt.axvline(v, color='black')

    return df_min, df_max, df_grouped, directions, scale, x, color_key


def read_and_transform_data(filename, group_by, to_plot, transforms=None, directions=None, scale=None, normalize=None,
                            color_key=None, sort_key=None, sync_scale=None):
    df = read_csv(filename)

    if transforms:
        # first, transform all existing series
        for k, func in transforms.items():
            if k in df.columns:
                df[k] = df[k].apply(func)

        # then, create new ones
        for k, func in transforms.items():
            if k not in df.columns:
                df[k] = func(df)

    if not directions:
        directions = [1] * len(to_plot)
    if not scale:
        scale = [None] * len(to_plot)

    usecols = to_plot + group_by

    if sort_key:
        if not isinstance(sort_key, list) and not isinstance(sort_key, tuple):
            sort_key = [sort_key]
        df.sort_values(sort_key, ascending=False, inplace=True)
        for k in sort_key:
            if k not in usecols:
                usecols.append(k)
    if color_key and color_key not in usecols:
        usecols.append(color_key)

    usecols = list(set(usecols))

    df_min = df[to_plot].min()
    df_max = df[to_plot].max()

    scale_dict = dict(zip(to_plot, scale))

    if sync_scale:
        for sync in sync_scale:
            sync_filtered = filter(lambda x: not isinstance(x, tuple), sync)
            scale_range = filter(lambda x: isinstance(x, tuple), sync)
            if len(scale_range):
                min_s, max_s = scale_range[0]
            else:
                min_s = df_min[sync_filtered].min()
                max_s = df_max[sync_filtered].max()
            for key in sync_filtered:
                df_min[key] = min_s
                df_max[key] = max_s
                scale_dict[key] = scale_dict[sync_filtered[0]]

    df_scaled = df[usecols].copy()
    df_min_scaled = df_min.copy()
    df_max_scaled = df_max.copy()

    # drop missing data - we won't be able to plot it anyway
    df_scaled.dropna(how='any', inplace=True)

    for k, s in scale_dict.items():
        if s == 'log2':
            df_scaled[k] = df[k].apply(numpy.log2)
            df_min_scaled[k] = numpy.log2(df_min_scaled[k])
            df_max_scaled[k] = numpy.log2(df_max_scaled[k])
        elif s == 'log10':
            df_scaled[k] = df[k].apply(numpy.log10)
            df_min_scaled[k] = numpy.log10(df_min_scaled[k])
            df_max_scaled[k] = numpy.log10(df_max_scaled[k])

    df_norm = df_scaled.copy()

    if normalize:
        df_norm[to_plot] = (df_scaled[to_plot] - df_min_scaled) / (
            df_max_scaled - df_min_scaled)

    for d, k in zip(directions, to_plot):
        if not d:
            df_norm[k] = 1 - df_norm[k]

    df_grouped = df_norm.groupby(group_by)

    return df_min, df_max, df_grouped, directions, scale


def line_plot(filename, plot_x, plot_y, group_by, label_lambda, transforms, sort_key, trend=False, texts=None):
    to_plot = [plot_x] + (plot_y if isinstance(plot_y, list) or isinstance(plot_y, tuple) else [plot_y])

    df_min, df_max, df_grouped, directions, scale = read_and_transform_data(filename, group_by, to_plot, transforms,
                                                                            sort_key=sort_key)
    plt.figure()

    no_groups = len(df_grouped)

    if texts is None:
        texts = dict(zip(to_plot, to_plot))

    for g, (index, group) in enumerate(df_grouped):
        x = group[plot_x]

        for k in plot_y:
            y = group[k]
            plt.plot(x, y, label=label_lambda(index, k))

    if trend:
        combined = df_grouped[[plot_x] + plot_y].apply(lambda a: a).groupby(plot_x).mean().reset_index()
        x = combined[plot_x]
        fit = numpy.polyfit(x, combined[plot_y], deg=2)
        plt.plot(x, fit[0] * x**2 + fit[1]*x + fit[2], label='', linestyle='dashed', color='black')

    plt.legend(loc='lower right')
    plt.gca().set_xlabel(texts[plot_x], family='serif')

    if no_groups > 1:
        plt.gca().set_ylabel(texts[plot_y[0]], family='serif')
