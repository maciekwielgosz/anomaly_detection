import sklearn.metrics
import numpy
import sys

from matplotlib import pyplot as plt, rc
from matplotlib.backends.backend_pdf import PdfPages

from analysta.utils import ResultsReader

rc('figure', figsize=(11.69, 8.27))

if __name__ == "__main__":
    setup_path = sys.argv[1]
    pdf_path = sys.argv[2]

    all_series = []

    reader = ResultsReader(setup_path)
    for row in reader:
        all_series.append({
            'look_back': row['preparation']['look_back'],
            'in_buckets': row['preparation']['in_buckets'],
            'in_bucketization': row['preparation']['in_bucketization'],
            'roc_curve_path': row['out']['model']['test']['roc_curve_path'],
            'pr_curve_path': row['out']['model']['test']['pr_curve_path']
        })

    axes_mapping = {
        'roc': {
            'x': 'fpr',
            'y': 'tpr'
        },
        'pr': {
            'x': 'recall',
            'y': 'precision'
        }
    }

    titles_mapping = {
        'roc': {
            'x': 'False Positive Rate',
            'y': 'True Positive Rate'
        },
        'pr': {
            'x': 'Recall',
            'y': 'Precision'
        }
    }

    with PdfPages(pdf_path) as pdf:
        for in_bucketization in ['adaptive', 'recursive_adaptive', 'cumulative_amplitude']:
            for curve in ['roc', 'pr']:
                fig = plt.figure()
                fig.suptitle('%s Curve (%s)' % (curve.upper(), in_bucketization, ))

                if curve == 'pr':
                    f_scores = numpy.linspace(0.2, 0.8, num=4)
                    lines = []
                    labels = []
                    for f_score in f_scores:
                        x = numpy.linspace(0.01, 1)
                        y = f_score * x / (2 * x - f_score)
                        plt.plot(x[y >= 0], y[y >= 0], color='gray', alpha=0.2)
                        plt.annotate('f1={0:0.1f}'.format(f_score), xy=(0.9, y[45] + 0.02))
                else:
                    plt.plot([0, 1], [0, 1], color='gray', alpha=0.2)

                filtered_series = [s for s in all_series if s['in_bucketization'] == in_bucketization]
                filtered_series.sort(key=lambda o: o['look_back'])
                for series in filtered_series:
                    data = numpy.load(series[curve + '_curve_path'])

                    x = data[axes_mapping[curve]['x']]
                    y = data[axes_mapping[curve]['y']]
                    auc = sklearn.metrics.auc(x, y)

                    plt.plot(x, y, label='look_back={}; AUC={:.4f}'.format(series['look_back'], auc))

                plt.xlim([0.0, 1.0])
                plt.ylim([0.0, 1.0])
                plt.xlabel(titles_mapping[curve]['x'])
                plt.ylabel(titles_mapping[curve]['y'])
                plt.legend()

                pdf.savefig(fig)
                plt.close(fig)
