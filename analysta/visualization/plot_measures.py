from collections import OrderedDict

import sys

from matplotlib import pyplot as plt, rc
from matplotlib.backends.backend_pdf import PdfPages

from analysta.utils import ResultsReader

MEASURES = ['accuracy', 'f1score', 'f2score']

rc('figure', figsize=(8.27, 11.69))

if __name__ == "__main__":
    setup_path = sys.argv[1]
    pdf_path = sys.argv[2]

    series = OrderedDict({
        'adaptive': [],
        'recursive_adaptive': [],
        'cumulative_amplitude': [],
        'random': []
    })
    min_values = {
        'accuracy': 1.0,
        'f1score': 1.0,
        'f2score': 1.0,
    }

    reader = ResultsReader(setup_path)
    for row in reader:
        obj = {
            'model': row['setup']['model'],
            'look_back': row['preparation']['look_back'],
            'in_buckets': row['preparation']['in_buckets'],
            'in_bucketization': row['preparation']['in_bucketization'],
            'accuracy': row['out']['model']['test']['acc'],
            'f1score': row['out']['model']['test']['f1score'],
            'f2score': row['out']['model']['test']['f2score']
        }
        if obj['model'] == 'analysta.utils.classes.models.x2y.RandomModel':
            series['random'].append(obj)
        else:
            series[obj['in_bucketization']].append(obj)
            for measure in MEASURES:
                min_values[measure] = min(min_values[measure], obj[measure])

    with PdfPages(pdf_path) as pdf:
        for measure in MEASURES:
            fig, axes = plt.subplots(4, 1, sharex=True, squeeze=True)
            fig.subplots_adjust(wspace=0, hspace=0.05)

            for index, (alg, data) in enumerate(series.items()):
                ax = axes[index]

                data.sort(key=lambda o: o['look_back'])

                grids = list(set([d['in_buckets'] for d in data]))
                grids.sort()

                if len(grids) > 1:
                    for g in grids:
                        x = [d['look_back'] for d in data if d['in_buckets'] == g]
                        y = [d[measure] for d in data if d['in_buckets'] == g]
                        ax.plot(x, y, label='in_buckets={}'.format(g))
                    grid_handles, grid_labels = ax.get_legend_handles_labels()
                    ax.set_ylim(min_values[measure], 1.0)
                    ax.set_title('GRU, \\textit{{{:s}}}'.format(alg.replace('_', '\\_')), usetex=True, loc='left',
                                 position=(0.01, 0.88))
                else:
                    x = [d['look_back'] for d in data]
                    y = [d[measure] for d in data]
                    ax.plot(x, y, '--', color='gray', label='random')
                    random_handles, random_labels = ax.get_legend_handles_labels()
                    pos = (0.01, 0.01) if y[0] > y[-1] else (0.01, 0.88)
                    ax.set_title('Random', usetex=True, loc='left',
                                 position=pos)

                ax.set_ylabel(measure)

            axes[0].legend(grid_handles + random_handles, grid_labels + random_labels, ncol=3, loc='lower right')
            plt.xlabel('look_back')
            pdf.savefig(fig)
            plt.close(fig)
