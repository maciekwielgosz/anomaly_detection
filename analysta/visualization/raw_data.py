import argparse
import os
import sys
import numpy

from matplotlib import pyplot as plt, rc, figure
from matplotlib.backends.backend_pdf import PdfPages
from matplotlib.patches import ConnectionPatch

from analysta.analyzer.analyzer import Analyzer
from analysta.visualization.utils import get_channels_info
from analysta.utils.config import Config
from analysta.utils.results import ResultsReader
from analysta.cli.arguments.types import readable_file
from analysta.data.utils import convert_to_grid
from analysta.cli.arguments.common import add_config_argument, add_report_argument, add_limit_argument
from analysta.data.loading import load_series_from_paths, get_series_loader, extract_series_name_from_path


def plot_spans(spans, axes):
    for span in spans:
        for ax in axes:
            ax.axvspan(span[0], span[1], facecolor='lightgray', alpha=0.5)


def plot_mark(mark, axes):
    con = ConnectionPatch((mark, axes[-1].get_ylim()[0]), (mark, axes[0].get_ylim()[1]),
                          "data", "data", axes[-1], axes[0],
                          linewidth=1, arrowstyle='-', color='r', connectionstyle='angle3', alpha=1)
    axes[-1].add_artist(con)


def plot_edges_span(edges, raw_data, channels, axes, zoom=True):
    for index, ax in enumerate(axes):
        e = get_edges_for_channel(edges, index)

        for i in range(0, e.shape[0], 2):
            ax.axhspan(e[i], e[i + 1], facecolor='0.1', alpha=0.5)

        adjust_axis_zoom(ax, channels, e, index, raw_data, zoom)


def get_edges_for_channel(edges, index):
    if edges.shape[1] % 2 == 1:
        e = numpy.append(edges[index], [1])
    else:
        e = edges[index]
    return e


def adjust_axis_zoom(ax, channels, e, index, raw_data, zoom):
    if zoom:
        e = numpy.append([0], e)
        hist, bins = numpy.histogram(raw_data[:, channels[index]], e)
        max_samples = numpy.argmax(hist)
        ax.set_ylim((bins[numpy.maximum(0, max_samples - 3)], bins[numpy.minimum(max_samples + 3, bins.shape[0] - 1)]))


def plot_edges_middle(edges, raw_data, channels, axes, zoom=True):
    quantized_data = convert_to_grid(raw_data[:, channels], edges)
    middles = Analyzer.get_middles_for_edges(edges)

    for index, ax in enumerate(axes):
        e = get_edges_for_channel(edges, index)

        quantized_data_values = middles[quantized_data[:, index]]
        ax.plot(quantized_data_values, 'o', markersize=1, label='grid=%i' % (edges.shape[1] + 1), alpha=0.5)

        adjust_axis_zoom(ax, channels, e, index, raw_data, zoom)


def plot_edges_bar(edges, raw_data, channels, axes, zoom=True):
    quantized_data = convert_to_grid(raw_data[:, channels], edges)
    edges_with_01 = numpy.concatenate(([[0]], edges, [[1]]), axis=1).reshape(-1, 1)

    for index, ax in enumerate(axes):
        e = get_edges_for_channel(edges, index)

        bottoms = edges_with_01[quantized_data[:, index]][:, index]
        tops = edges_with_01[quantized_data[:, index] + 1][:, index]

        ax.bar(numpy.arange(len(quantized_data)), tops - bottoms, bottom=bottoms,
               label='grid=%i' % (edges.shape[1] + 1), alpha=0.5, linewidth=0, color='orange')

        adjust_axis_zoom(ax, channels, e, index, raw_data, zoom)


def plot_series(raw_data, channels, channels_names, series_name, pdf, mark=None, spans=None, x=None, edges=None,
                edges_plot_type='span', edges_plot_zoom=True, samples_limit=None):

    fig = figure.Figure()
    axes = fig.subplots(len(channels), 1, sharex='all')

    fig.suptitle(series_name)

    if len(axes) < 2:
        axes = (axes, )

    for row, c in enumerate(channels):
        ax = axes[row]

        if x is None:
            ax.plot(raw_data[:samples_limit, c], label=channels_names[c])
        else:
            ax.plot(x, raw_data[:samples_limit, c], label=channels_names[c])

    axes[-1].set_xlabel('samples')

    if spans is not None:
        plot_spans(spans, axes)

    if edges is not None:
        if edges_plot_type == 'span':
            plot_edges_span(edges, raw_data, channels, axes, edges_plot_zoom)
        elif edges_plot_type == 'middle':
            plot_edges_middle(edges, raw_data, channels, axes, edges_plot_zoom)
        elif edges_plot_type == 'bar':
            plot_edges_bar(edges, raw_data, channels, axes, edges_plot_zoom)

    if mark is not None:
        plot_mark(mark, axes)

    for ax in axes:
        ax.legend()

    pdf.savefig(fig)
    plt.close(fig)


def run_for_data_path(raw_data, series_name, input_channels, output_channels, channels_names, edges, pdf,
                      edges_plot_type='span', edges_plot_zoom=True, mark_channel=None, samples_limit=None):
    quench_mark = None
    if mark_channel is None:
        quench_mark = find_quench_mark(raw_data[:, output_channels])
    elif mark_channel >= 0:
        quench_mark = find_quench_mark(raw_data[:, mark_channel])

    plot_series(raw_data, input_channels, channels_names, series_name, pdf, mark=quench_mark, edges=edges,
                edges_plot_type=edges_plot_type, edges_plot_zoom=edges_plot_zoom, samples_limit=samples_limit)

    return 1 if quench_mark is not None else 0


def find_quench_mark(data):
    quench_mark = None
    points = numpy.nonzero(numpy.diff(numpy.concatenate(([[0]], data.reshape(1, -1)), axis=1)))[1]

    if len(points):
        quench_mark = points[0]
    return quench_mark


def run(config, edges_path=None, limit=None, edges_plot_type='span', edges_plot_zoom=True, mark_channel=None,
        data_set='train', samples_limit=None):
    if edges_path is None:
        edges_path = config.get('preparation.edges_path')

    edges = numpy.load(edges_path)['in_edges'] if edges_path else None

    print('Edges: ', edges)

    paths_value = config.get('data.{:s}_paths'.format(data_set))

    series_loader = get_series_loader(config)

    data_paths = config.load_paths('data.{:s}_paths'.format(data_set))[:limit]
    channels_names, input_channels, output_channels = get_channels_info(config)
    marks_count = 0
    raw_data, series_names = load_series_from_paths(data_paths, series_loader)
    normal_series_paths = []

    if not isinstance(paths_value, list):
        paths_value = os.path.splitext(os.path.basename(paths_value))[0]
    else:
        paths_value = os.path.splitext(os.path.basename(config.path))[0]

    if len(input_channels) > 1:
        rc('figure', figsize=(8.27, 11.69))
    else:
        rc('figure', figsize=(11.69, 8.27))

    with PdfPages('%s_data_visualization.pdf' % (paths_value, )) as pdf:
        for name, series in zip(series_names, raw_data):
            mark = run_for_data_path(series, name, input_channels, output_channels, channels_names, edges, pdf,
                                     edges_plot_type, edges_plot_zoom, mark_channel, samples_limit)
            marks_count += mark
            if mark == 0:
                normal_series_paths.append(data_paths[name])

        if edges is not None:
            concatenated = numpy.concatenate(raw_data)
            plot_grid_histograms(concatenated, edges, input_channels, channels_names, pdf)

    print("%i series total" % (len(data_paths), ))
    print("%i series with quench mark" % (marks_count, ))

    with open('{:s}_no_marks.txt'.format(data_set), 'w') as tnm:
        for path in normal_series_paths:
            tnm.write(os.path.basename(path))
            tnm.write("\n")


def plot_grid_histograms(concatenated, edges, input_channels, channels_names, pdf):
    for index, c in enumerate(input_channels):
        fig = plt.figure()
        fig.suptitle('Grid histogram for %s' % (channels_names[c]))

        grid = len(edges[index]) + 1

        hist, bins = numpy.histogram(
            concatenated[:, index],
            bins=numpy.concatenate(([0], edges[index], [1]))
        )

        plt.bar(numpy.arange(grid), hist, figure=fig)
        plt.gca().set_yscale('log')
        plt.xticks(numpy.arange(grid), ["%.4e" % b for b in bins[1:]], rotation=90, ha="left")
        pdf.savefig(fig)
        plt.close(fig)


def parse_data(args):
    parser = argparse.ArgumentParser(
        description="Raw data visualization")

    group = parser.add_mutually_exclusive_group(required=True)

    add_config_argument(group, False)
    add_report_argument(group, False)
    add_limit_argument(parser, help_text='series limit', metavar='SERIES_LIMIT')

    parser.add_argument(
        '-n',
        '--samples-limit',
        type=int,
        default=None,
        help='plotted samples per plot limit'
    )

    parser.add_argument(
        '-e',
        '--edges',
        type=readable_file,
        metavar='EDGES_FILE',
        default=None
    )

    parser.add_argument(
        '-t',
        '--type',
        type=str,
        metavar='EDGES_PLOT_TYPE',
        choices=['span', 'middle', 'bar', 'none'],
        default='span'
    )

    parser.add_argument(
        '--no-zoom',
        action='store_true'
    )

    parser.add_argument(
        '-m',
        '--mark',
        type=int,
        metavar='MARK_CHANNEL',
        default=None,
        help='Set to negative value to disable plotting'
    )
    
    parser.add_argument(
        '-s',
        '--dataset',
        type=str,
        default='train'
    )

    return parser.parse_args(args)


if __name__ == "__main__":
    args = parse_data(sys.argv[1:])

    if args.config is not None:
        loaded_config = Config(args.config)
    else:
        reader = ResultsReader(args.setup)
        loaded_config = Config()
        for reader_row in reader:
            loaded_config = loaded_config.copy(reader_row)
            break

    run(loaded_config, args.edges, args.limit, args.type, not args.no_zoom, args.mark, args.dataset, args.samples_limit)
