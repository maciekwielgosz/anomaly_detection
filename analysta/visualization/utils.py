import argparse
import logging

import numpy

from analysta.cli.arguments.common import add_limit_argument, add_report_argument
from analysta.skeleton import setup_logging
from analysta.utils.config import Config

from analysta.cli.arguments.types import readable_file


def parse_args(args, kind):
    parser = argparse.ArgumentParser(
        description="Results visualization for %s data" % (kind, ))

    add_report_argument(parser)
    add_limit_argument(parser, metavar='SERIES_LIMIT', help_text='series limit')

    parser.add_argument(
        '-o',
        '--real',
        type=lambda x: x if x.isdigit() or readable_file(x) else argparse.ArgumentTypeError(),
        metavar='REAL_ANOMALIES_FILE or REAL_ANOMALIES_CHANNEL_INDEX',
        default=None
    )

    return parser.parse_args(args)


def get_channels_info(config):
    input_channels = config.get('data.input_channels')
    output_channels = config.get('data.output_channels')
    channels_names = config.get('data.channels_names')
    return channels_names, input_channels, output_channels
