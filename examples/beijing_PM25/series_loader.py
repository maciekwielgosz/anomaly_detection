import csv
import datetime
import numpy


def to_float(val):
    try:
        return float(val)
    except ValueError:
        return float("NaN")


def series_loader(file_path, mmap_mode='r'):
    """
    Load Beijing PM2.5 data and convert it into usable format
    :param file_path: data series *.csv file path
    :param mmap_mode: ignored, for compatibility only
    :return:
    """

    with open(file_path) as csvfile:
        reader = csv.DictReader(csvfile)

        converted = []

        for row in reader:
            date = datetime.datetime(
                int(row['year']), int(row['month']),
                int(row['day']), int(row['hour'])
            )

            row['cbwd'] = row['cbwd'].upper()
            NW = (row['cbwd'] == 'NW')
            NE = (row['cbwd'] == 'NE')
            SE = (row['cbwd'] == 'SE')
            SW = (row['cbwd'] == 'SW')
            CV = (row['cbwd'] == 'CV')

            converted.append([
                date,
                to_float(row['pm2.5']),
                float(row['DEWP']),
                float(row['TEMP']),
                float(row['PRES']),
                NW,
                NE,
                SE,
                SW,
                CV,
                float(row['Iws']),
                float(row['Is']),
                float(row['Ir'])
            ])

    return numpy.array(converted)
