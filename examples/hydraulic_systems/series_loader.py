import itertools

import numpy
import os

SENSORS_100 = ["PS1", "PS2", "PS3", "PS4", "PS5", "PS6", "EPS1"]  # 100Hz
SENSORS_10 = ["FS1", "FS2"]  # 10Hz
SENSORS_1 = ["TS1", "TS2", "TS3", "TS4", "VS1", "CE", "CP", "SE"]  # 1Hz
SENSORS = SENSORS_100 + SENSORS_10 + SENSORS_1


def to_float(val):
    try:
        return float(val)
    except ValueError:
        return float("NaN")


def line_to_ndarray(contents, idx, key, repeats=1):
    return numpy.repeat([to_float(val) for val in contents[key][idx].split('\t')], repeats)


def series_loader(file_paths, mmap_mode='r'):
    data = []
    lengths = []
    contents = {}

    for path in file_paths:
        with open(path, 'r') as h:
            key = os.path.splitext(os.path.basename(path))[0]
            contents[key] = list(h)
            lengths.append(len(contents[key]))

    series_len = min(lengths)

    if series_len != max(lengths):
        raise ValueError('Files contain variable amount of series: {}'.format(
            dict(zip(contents.keys(), lengths)))
        )

    for idx in range(series_len):
        series = []

        for key in SENSORS_100:
            series.append(line_to_ndarray(contents, idx, key, 1))

        for key in SENSORS_10:
            series.append(line_to_ndarray(contents, idx, key, 10))

        for key in SENSORS_1:
            series.append(line_to_ndarray(contents, idx, key, 100))

        for output in contents['profile'][idx].split('\t'):
            series.append(numpy.repeat([int(output)], series[0].size))

        data.append(numpy.array(series).T)  # samples x channels

    return data
