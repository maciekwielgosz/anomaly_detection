import numpy as np

from analysta.data.sequences.readers import RawReader


def test_chunk_size_one():
    data = [np.random.random((149, 3)), np.random.random((133, 3))]

    reader = RawReader(name='default', data=data, in_channels=[0, 1], out_channels=[2], labels_channel=2,
                       look_back=128, batch_size=4, samples_percentage=1, return_indexes=False,
                       shuffle=False, chunk_size=1, look_ahead=1, allow_random=False, required_history=128)

    batch_x, batch_y, _ = reader.calc_item(0)
    assert np.array_equal(batch_x[0], data[0][0:128, 0:2])
    assert np.array_equal(batch_y[0], data[0][128, [2]])
    assert np.array_equal(batch_x[1], data[0][1:129, 0:2])
    assert np.array_equal(batch_y[1], data[0][129, [2]])

    batch_x, batch_y, _ = reader.calc_item(5)
    assert np.array_equal(batch_x[0], data[0][20:148, 0:2])
    assert np.array_equal(batch_y[0], data[0][148, [2]])


def test_bigger_chunk_size():
    data = [np.random.random((149, 3)), np.random.random((133, 3))]

    reader = RawReader(name='default', data=data, in_channels=[0, 1], out_channels=[2], labels_channel=2,
                       look_back=128, batch_size=4, samples_percentage=1, return_indexes=False,
                       shuffle=False, chunk_size=4, look_ahead=1, allow_random=False, required_history=128)

    batch_x, batch_y, _ = reader.calc_item(0)
    assert np.array_equal(batch_x[0], data[0][0:128, 0:2])
    assert np.array_equal(batch_y[0], data[0][128, [2]])
    assert np.array_equal(batch_x[1], data[0][1:129, 0:2])
    assert np.array_equal(batch_y[1], data[0][129, [2]])

    batch_x, batch_y, _ = reader.calc_item(5)
    assert np.array_equal(batch_x[0], data[1][0:128, 0:2])
    assert np.array_equal(batch_y[0], data[1][128, [2]])
