from sklearn.metrics import r2_score, mean_absolute_error, mean_squared_log_error, mean_squared_error, confusion_matrix, \
    accuracy_score, precision_recall_fscore_support
from sklearn.utils import compute_class_weight

from analysta.metrics import root_mean_squared_error, mean_absolute_percentage_error, root_mean_squared_log_error
from analysta.metrics.from_partials import *


def run_regression_test(pred_data, real_data):
    warnings.filterwarnings('error')

    axes = (0, 1, 2)
    partials = {
        'observed_samples': real_data.shape[0] * real_data.shape[1] * real_data.shape[2],
        'ss_tot': total_sum_of_squares(axes, real_data, pred_data, 0),
        'ss_reg': regression_sum_of_squares(axes, real_data, pred_data, 0),
        'ss_res': residual_sum_of_squares(axes, real_data, pred_data, 0),
        'ss_log_res': log_residual_sum_of_squares(axes, real_data, pred_data, 0),
        's_res': sum_of_residuals(axes, real_data, pred_data, 0),
        's_frac': sum_of_fractionals(axes, real_data, pred_data, 0)
    }

    metrics = regression_metrics(**partials)

    try:
        r2 = r2_score(real_data, pred_data)
        assert numpy.allclose(metrics['r2'], r2, equal_nan=True)
    except (Warning, ValueError):
        pass
    try:
        mae = mean_absolute_error(real_data, pred_data)
        assert numpy.allclose(metrics['mae'], mae, equal_nan=True)
    except (Warning, ValueError):
        pass
    try:
        mse = mean_squared_error(real_data, pred_data)
        assert numpy.allclose(metrics['mse'], mse, equal_nan=True)
    except (Warning, ValueError):
        pass
    try:
        rmse = root_mean_squared_error(real_data, pred_data)
        assert numpy.allclose(metrics['rmse'], rmse, equal_nan=True)
    except (Warning, ValueError):
        pass
    try:
        msle = mean_squared_log_error(real_data, pred_data)
        assert numpy.allclose(metrics['msle'], msle, equal_nan=True)
    except (Warning, ValueError):
        pass
    try:
        rmsle = root_mean_squared_log_error(real_data, pred_data)
        assert numpy.allclose(metrics['rmsle'], rmsle, equal_nan=True)
    except (Warning, ValueError):
        pass
    try:
        mape = mean_absolute_percentage_error(real_data, pred_data)
        assert numpy.allclose(metrics['mape'], mape, equal_nan=True)
    except (Warning, ValueError):
        pass


def test_regression_metrics_single_channel():
    # 16 chunks of 256 length, single predicted sample, single channel
    real_data = numpy.random.random((16, 256, 1, 1)) * 3 - 1.5

    # add some noise
    pred_data = real_data + numpy.random.random(real_data.shape) * 0.2 - 0.1

    run_regression_test(pred_data, real_data)


def test_regression_metrics_multi_channel():
    # 16 chunks of 256 length, single predicted sample, 4 channels
    real_data = numpy.random.random((16, 256, 1, 4)) * 3 - 1.5

    # add some noise
    pred_data = real_data + numpy.random.random(real_data.shape) * 0.2 - 0.1

    run_regression_test(pred_data, real_data)


def test_regression_metrics_multi_out_multi_channel():
    # 16 chunks of 256 length, 128 predicted samples, 4 channels
    real_data = numpy.random.random((16, 256, 128, 4)) * 3 - 1.5

    # add some noise
    pred_data = real_data + numpy.random.random(real_data.shape) * 0.2 - 0.1

    run_regression_test(pred_data, real_data)


def test_compute_balanced_weights():
    real_data = numpy.random.randint(0, 2, (16, 256, 1, 1))

    positives = numpy.count_nonzero(real_data)
    negatives = real_data.size - positives

    bw = compute_balanced_weights(2, numpy.array([negatives, positives]), real_data.size)
    sklearn_bw = compute_class_weight('balanced', [0, 1], real_data.reshape((-1,)).tolist())
    assert numpy.allclose(bw, sklearn_bw, equal_nan=True)


def test_classification_metrics_binary_single_channel():
    real_data = numpy.random.randint(0, 2, (16, 256, 1, 1))
    pred_data = numpy.random.randint(0, 2, (16, 256, 1, 1))

    cm = confusion_matrix(real_data.reshape((-1, 1)),
                          pred_data.reshape((-1, 1)), labels=list(range(2)))

    metrics_no_average = classification_metrics(cm, average=None)
    metrics_micro = classification_metrics(cm, average='micro')
    metrics_macro = classification_metrics(cm, average='macro')
    metrics_weighted = classification_metrics(cm, average='weighted')
    metrics_binary = classification_metrics(cm, average='binary')

    # accuracy should be the same no matter the averaging
    acc = accuracy_score(real_data.reshape((-1, 1)), pred_data.reshape((-1, 1)))
    assert numpy.allclose(metrics_no_average['acc'], acc)
    assert numpy.allclose(metrics_micro['acc'], acc)
    assert numpy.allclose(metrics_macro['acc'], acc)
    assert numpy.allclose(metrics_weighted['acc'], acc)
    assert numpy.allclose(metrics_binary['acc'], acc)

    precision, recall, f1score, support = precision_recall_fscore_support(
        real_data.reshape((-1, 1)), pred_data.reshape((-1, 1)),
        average=None
    )
    assert numpy.allclose(metrics_no_average['precision'], precision)
    assert numpy.allclose(metrics_no_average['recall'], recall)
    assert numpy.allclose(metrics_no_average['f1score'], f1score)
    assert numpy.array_equiv(metrics_no_average['support'], support)

    precision, recall, f1score, support = precision_recall_fscore_support(
        real_data.reshape((-1, 1)), pred_data.reshape((-1, 1)),
        average='micro'
    )
    assert numpy.allclose(metrics_micro['precision'], precision)
    assert numpy.allclose(metrics_micro['recall'], recall)
    assert numpy.allclose(metrics_micro['f1score'], f1score)
    # precision_recall_fscore_support returns support=None if average!=None

    precision, recall, f1score, support = precision_recall_fscore_support(
        real_data.reshape((-1, 1)), pred_data.reshape((-1, 1)),
        average='macro'
    )
    assert numpy.allclose(metrics_macro['precision'], precision)
    assert numpy.allclose(metrics_macro['recall'], recall)
    assert numpy.allclose(metrics_macro['f1score'], f1score)
    # precision_recall_fscore_support returns support=None if average!=None

    precision, recall, f1score, support = precision_recall_fscore_support(
        real_data.reshape((-1, 1)), pred_data.reshape((-1, 1)),
        average='weighted'
    )
    assert numpy.allclose(metrics_weighted['precision'], precision)
    assert numpy.allclose(metrics_weighted['recall'], recall)
    assert numpy.allclose(metrics_weighted['f1score'], f1score)
    # precision_recall_fscore_support returns support=None if average!=None

    precision, recall, f1score, support = precision_recall_fscore_support(
        real_data.reshape((-1, 1)), pred_data.reshape((-1, 1)),
        average='binary'
    )
    assert numpy.allclose(metrics_binary['precision'], precision)
    assert numpy.allclose(metrics_binary['recall'], recall)
    assert numpy.allclose(metrics_binary['f1score'], f1score)
    # precision_recall_fscore_support returns support=None if average!=None


def test_classification_metrics_multiple_classes_single_channel():
    real_data = numpy.random.randint(0, 4, (16, 256, 1, 1))
    pred_data = numpy.random.randint(0, 4, (16, 256, 1, 1))

    cm = confusion_matrix(real_data.reshape((-1, 1)),
                          pred_data.reshape((-1, 1)), labels=list(range(4)))

    metrics_no_average = classification_metrics(cm, average=None)
    metrics_micro = classification_metrics(cm, average='micro')
    metrics_macro = classification_metrics(cm, average='macro')
    metrics_weighted = classification_metrics(cm, average='weighted')

    acc = accuracy_score(real_data.reshape((-1, 1)), pred_data.reshape((-1, 1)))
    assert numpy.allclose(metrics_no_average['acc'], acc)
    assert numpy.allclose(metrics_micro['acc'], acc)
    assert numpy.allclose(metrics_macro['acc'], acc)
    assert numpy.allclose(metrics_weighted['acc'], acc)

    precision, recall, f1score, support = precision_recall_fscore_support(
        real_data.reshape((-1, 1)), pred_data.reshape((-1, 1)),
        average=None
    )
    assert numpy.allclose(metrics_no_average['precision'], precision)
    assert numpy.allclose(metrics_no_average['recall'], recall)
    assert numpy.allclose(metrics_no_average['f1score'], f1score)
    assert numpy.array_equiv(metrics_no_average['support'], support)

    precision, recall, f1score, support = precision_recall_fscore_support(
        real_data.reshape((-1, 1)), pred_data.reshape((-1, 1)),
        average='micro'
    )
    assert numpy.allclose(metrics_micro['precision'], precision)
    assert numpy.allclose(metrics_micro['recall'], recall)
    assert numpy.allclose(metrics_micro['f1score'], f1score)
    # precision_recall_fscore_support returns support=None if average!=None

    precision, recall, f1score, support = precision_recall_fscore_support(
        real_data.reshape((-1, 1)), pred_data.reshape((-1, 1)),
        average='macro'
    )
    assert numpy.allclose(metrics_macro['precision'], precision)
    assert numpy.allclose(metrics_macro['recall'], recall)
    assert numpy.allclose(metrics_macro['f1score'], f1score)
    # precision_recall_fscore_support returns support=None if average!=None

    precision, recall, f1score, support = precision_recall_fscore_support(
        real_data.reshape((-1, 1)), pred_data.reshape((-1, 1)),
        average='weighted'
    )
    assert numpy.allclose(metrics_weighted['precision'], precision)
    assert numpy.allclose(metrics_weighted['recall'], recall)
    assert numpy.allclose(metrics_weighted['f1score'], f1score)
    # precision_recall_fscore_support returns support=None if average!=None

